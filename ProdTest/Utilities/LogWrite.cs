﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace Utilities
{
    public class LogWrite
    {
        private string m_exePath = string.Empty;
        private string dts = string.Empty;

        public LogWrite(string logMessage)
        {
            LogWriter(logMessage);
        }

        public LogWrite(string logMessage, bool useTime)
        {
            dts = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": ";
            LogWriter(logMessage);
        }


        public void LogWriter(string logMessage)
        {
            m_exePath = Environment.CurrentDirectory;
            try
            {
                using (StreamWriter w = File.AppendText(m_exePath + "\\" + "log.txt"))
                {
                    Log(logMessage, w);
                }
            }
            catch (Exception)
            {
            }
        }

        public void Log(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("{0}{1}", dts, logMessage);
/*                txtWriter.Write("\r\nLog Entry : ");
                txtWriter.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                    DateTime.Now.ToLongDateString());
                txtWriter.WriteLine("  :");
                txtWriter.WriteLine("  :{0}", logMessage);
                txtWriter.WriteLine("-------------------------------");
 */ 
            }
            catch (Exception)
            {
            }
        }

        public void LogLine(string logMessage, TextWriter txtWriter)
        {
            try
            {
                txtWriter.Write("{0}{1}", dts, logMessage);
            }
            catch (Exception)
            {
            }
        }
 
    
    }


    public class LogIt
    {
        public LogIt(string message, bool useDate, string path, string filename)
        {
            try
            {
                if (path == null) path = Environment.CurrentDirectory;

                string s = string.Empty;
                if (useDate) s += DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss") + ": ";
                s += message;

                using (StreamWriter w = File.AppendText(path + "\\" + filename))
                {
                    w.WriteLine("{0}", s);
                }
            }
            catch (Exception)
            {
            }
        }

        public LogIt(string message, bool useDate, string path) : this(message, useDate, path, "log.txt") { }
        public LogIt(string message, bool useDate) : this(message, useDate, null) {}
        public LogIt(string message, string path) : this(message, false, path) {}
        public LogIt(string message) : this (message, false, null) {}
    }

}
