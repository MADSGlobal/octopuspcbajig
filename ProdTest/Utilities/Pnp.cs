﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Management;

namespace Utilities
{
    public class Pnp
    {
        /// <summary>
        /// Returns a list of Pnp devices that match the filter condition
        /// </summary>
        /// <param name="filter">Must be containes in the "caption" field of the Pnp device</param>
        /// <returns>List of devices that match the filter</returns>
        public static List<PnpDeviceInfo> ReadPnpDevices(string filter)
        {
            List<PnpDeviceInfo> PnpList = new List<PnpDeviceInfo>();

            using (ManagementObjectSearcher devicelist = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity"))
            {
                if (devicelist == null) return PnpList;

                foreach (ManagementObject device in devicelist.Get())
                {
                    try
                    {
                        if (device["Caption"].ToString().Contains(filter))
                        {
                            PnpDeviceInfo PnpDevice = new PnpDeviceInfo();

                            PnpDevice.Caption = (string)device.GetPropertyValue("Caption");
                            PnpDevice.Name = (string)device.GetPropertyValue("Name");
                            PnpDevice.Service = (string)device.GetPropertyValue("Service");
                            PnpDevice.Description = (string)device.GetPropertyValue("Description");
                            PnpDevice.Manufacturer = (string)device.GetPropertyValue("Manufacturer");
                            PnpDevice.DeviceID = (string)device.GetPropertyValue("DeviceID");
                            PnpDevice.PNPDeviceID = (string)device.GetPropertyValue("PNPDeviceID");
                            PnpList.Add(PnpDevice);

                        }
                    }
                    catch (Exception) { }
                }
            }
            return PnpList;
        }

        /// <summary>
        /// Returns a list of Pnp devices where the "Caption" contains "(COM". Also sets the port field.
        /// </summary>
        /// <returns></returns>
        public static List<PnpDeviceInfo> ReadPnpComPorts()
        {
            var PnpList = ReadPnpDevices("(COM");

            if (PnpList.Count == 0) return PnpList;

            foreach (PnpDeviceInfo device in PnpList)
            {
                try
                {

                    device.Port = device.Caption.Substring(device.Caption.LastIndexOf("(COM"))
                        .Replace("(", string.Empty)
                        .Replace(")", string.Empty);
                }
                catch (Exception) { }
            }
            return PnpList;
        }


        /// <summary>
        /// Finds the first COM port that contains the name
        /// </summary>
        /// <param name="name">String that must be contained in the port's name</param>
        /// <returns>First matching COM port</returns>
        public static string FindComPortByName(string name)
        {
            var Uarts = ReadPnpComPorts();

            foreach (PnpDeviceInfo dev in Uarts)
            {
                if (dev.Name.Contains(name)) return dev.Port;
            }
            return string.Empty;
        }


        public class PnpDeviceInfo
        {
            public string Caption { get; internal set; }
            public string Name { get; internal set; }
            public string Service { get; internal set; }
            public string Description { get; internal set; }
            public string Manufacturer { get; internal set; }
            public string DeviceID { get; internal set; }
            public string PNPDeviceID { get; internal set; }
            public string Port { get; internal set; }

            public PnpDeviceInfo()
            {
                Caption = string.Empty;
                Name = string.Empty;
                Service = string.Empty;
                Description = string.Empty;
                Manufacturer = string.Empty;
                DeviceID = string.Empty;
                Port = string.Empty;
            }
        }


    }


}
