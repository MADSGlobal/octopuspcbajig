﻿using System.Timers;

namespace Utilities
{
    /// <summary>
    ///     Simple
    /// </summary>
    public class Delay
    {
        public bool HasExpired;

        /// <summary>
        ///     Simple delay timer, based on MSDN example. Does NOT update UI!
        /// </summary>
        /// <param name="ms">time to delay in ms</param>
        public Delay(double ms)
        {
            // Create a timer 
            var delayTimer = new Timer(ms);

            // Hook up the Elapsed event for the timer.
            delayTimer.Elapsed += (source, e) => { HasExpired = true; };

            delayTimer.AutoReset = false;
            delayTimer.Start();
            HasExpired = false;
            while (!HasExpired)
            {
            }
        }
    }
}