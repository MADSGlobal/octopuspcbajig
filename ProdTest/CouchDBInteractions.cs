﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

namespace CouchDbAPI
{
    [Serializable()]
    public class IkeDataModel
    {
        public string formhash { get; set; }
        public string time { get; set; }

    }

    
    public class CouchDBInteractions
    {

        private string _couchConnectionString = "http://localhost:5984/octopuspcbajig/";

        private Dictionary<string, string> resultSet;

        public CouchDBInteractions()
        {
            resultSet = new Dictionary<string, string>();
        }

        public CouchDBInteractions(string couchConnectionString) : this()
        {
            _couchConnectionString = couchConnectionString;
        }

        // new value will overwrite old one
        public void Add(string key, string value)
        {
            string oldvalue;

            if (!(resultSet.TryGetValue(key, out oldvalue)))
            {
                resultSet.Add(key, value);
            }
            else
            {
                Console.WriteLine("WARNING: " + key + " already exists in CouchDB document");
            }
        }

        //! Add error checking
        public void Flush()
        {
            using (var client = new WebClient())
            {
                try
                {
                    var t = new JavaScriptSerializer();
                    client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                    var ty = t.Serialize(resultSet);
                    var response = client.UploadString(_couchConnectionString, "post", ty);
                }
                catch (Exception ex)
                {
                    if (ex.Source != null)
                        Console.WriteLine("Exception: {0}", ex);
                }
            }
//            String document = "{ \"source\":\"ikedata\", \"target\":\"https://couch:r995QiRb2QWgXCy@production-couch.ikegps.net/pcbajig\"}";
            String document = "{ \"source\":\"octopuspcbajig\", \"target\":\"https://couch:r995QiRb2QWgXCy@production-couch.ikegps.net/octopuspcbajig\"}";

        
            var destination = new Uri("http://localhost:5984/_replicate");
            using (var client = new WebClient())
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                try
                {
                    client.UploadStringAsync(destination, "post", document);
                }
                catch (WebException e)
                {
                    if (e.Source != null)
                        Console.WriteLine("Exception: {0}", e);
                }

            }
        }
    }
}
