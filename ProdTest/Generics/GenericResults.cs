﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Generics.Result
{
    public class GenericResult 
    {
        private bool _isOK;
        private string _errorString = string.Empty;

        /// <summary>
        /// Return class, containing of a success flag, an error description and 0 or more return values
        /// </summary>
        /// <param name="isOK">Success flag</param>
        /// <param name="errorString">error description</param>
        public GenericResult(bool isOK, string errorString)
        {
            _isOK = isOK;
            _errorString = errorString;
        }

        public GenericResult(bool isOK) : this(isOK, string.Empty)
        {}  

        /// <summary>
        /// Success of the operation
        /// </summary>
        public bool isOK
        {
            get { return _isOK; }
        }

        /// <summary>
        /// Error description or empty string
        /// </summary>
        public string errorString
        {
            get { return 
                (_errorString == null) 
                ? string.Empty 
                : _errorString ; 
            }
        }

        /// <summary>
        /// Returns a string that represents the current object
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (_isOK) return _isOK.ToString();
            return _isOK.ToString() + ", " + _errorString;
        }

    }


    public class GenericResult<T> : GenericResult
    {
        private T _result;

        public GenericResult(bool isOK, string errorString, T result) : base(isOK, errorString)
        {
            _result = result;
        }

        public T result
        {
            get { return _result; }
        }
    }


    public class GenericResult<T, U> : GenericResult<T>
    {
        private U _result;

        public GenericResult(bool isOK, string errorString, T result, U result2) : base(isOK, errorString, result)
        {
            _result = result2;
        }

        public U result2
        {
            get { return _result; }
        }
    }


    public class GenericResult<T, U, V> : GenericResult<T, U>
    {
        private V _result;

        public GenericResult(bool isOK, string errorString, T result, U result2, V result3) : base(isOK, errorString, result, result2)
        {
            _result = result3;
        }

        public V result3
        {
            get { return _result; }
        }
    }

}
