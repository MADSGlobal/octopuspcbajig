using System;
using System.Collections.Generic;
using System.Text;

namespace GenericEventArgs
{
    public class ValueEventArgs<T> : EventArgs
    {
        public ValueEventArgs(T value)
        {
            m_value = value;
        }

        private T m_value;

        public T Value
        {
            get { return m_value; }
        }
    }

    public class ValueEventArgs<T, U> : ValueEventArgs<T>
    {
        public ValueEventArgs(T value, U value2)
            : base(value)
        {
            m_value2 = value2;
        }

        private U m_value2;

        public U Value2
        {
            get { return m_value2; }
        }
    }

    public class ValueEventArgs<T, U, V> : ValueEventArgs<T, U>
    {
        public ValueEventArgs(T value, U value2, V value3)
            : base(value, value2)
        {
            m_value3 = value3;
        }

        private V m_value3;

        public V Value3
        {
            get { return m_value3; }
        }
    } 
}
