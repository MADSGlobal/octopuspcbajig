﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ProdTest
{
    public partial class OperatorActionRequest : Form
    {
        public bool operatorActionRequestCancelled = false;

        public OperatorActionRequest()
        {
            InitializeComponent();
        }

        private void btnEnterOperator_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
            operatorActionRequestCancelled = true;
            this.Close();
        }

    }
}
