﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CoreScanner;
using Generics.Result;
using GenericEventArgs;
using System.Diagnostics;
using System.Xml;
using System.Text.RegularExpressions;


namespace ProdTest.HardwareWrapper
{
    public class BarcodeScanner
    {
        public event EventHandler<ValueEventArgs<string[]>> BarcodeScannerEvent;

        private static CCoreScannerClass cCoreScannerClass;

        public GenericResult Init()
        {
            try
            {
                //Instantiate CoreScanner Class
                cCoreScannerClass = new CCoreScannerClass();

                //Call Open API
                var scannerTypes = new short[1]; //Scanner Types you are interested in
                scannerTypes[0] = 1; // 1 for all scanner types
                short numberOfScannerTypes = 1; // Size of the scannerTypes array
                int status; // Extended API return code
                cCoreScannerClass.Open(0, scannerTypes, numberOfScannerTypes, out status);

                // Add scanner event
                cCoreScannerClass.BarcodeEvent += cCoreScannerClass_BarcodeEvent;

                // Let's subscribe for events
                int opcode = 1001; // Method for Subscribe events
                string outXML; // XML Output
                string inXML = "<inArgs>" +
                               "<cmdArgs>" +
                               "<arg-int>1</arg-int>" + // Number of events you want to subscribe
                               "<arg-int>1</arg-int>" + // Comma separated event IDs
                               "</cmdArgs>" +
                               "</inArgs>";
                cCoreScannerClass.ExecCommand(opcode, ref inXML, out outXML, out status);
                return new GenericResult(true, null);
            }
            catch (SystemException ex)
            {
                return new GenericResult(false, ex.Source + "\r\n" + ex.Message);
            }
        }


        private void cCoreScannerClass_BarcodeEvent(short eventType, ref string pscanData)
        {
            string barcode = pscanData;

            string[] barcodeline = GetBarcodeLabel(barcode);
            for (int i = 0; i < barcodeline.Length; i++)
            {
                if (barcodeline[i] != null) barcodeline[i] = barcodeline[i].Trim();
            }

            if (BarcodeScannerEvent != null) BarcodeScannerEvent(this, new ValueEventArgs<string[]>(barcodeline));
        }


        private static string[] GetBarcodeLabel(string strXml)
        {
            Debug.WriteLine("Initial XML" + strXml);
            var xmlDoc = new XmlDocument();
            xmlDoc.LoadXml(strXml);

            // Convert Xml to str
            string strData = String.Empty;
            string barcode = xmlDoc.DocumentElement.GetElementsByTagName("datalabel").Item(0).InnerText;
            string symbology = xmlDoc.DocumentElement.GetElementsByTagName("datatype").Item(0).InnerText;
            string[] numbers = barcode.Split(' ');

            foreach (string number in numbers)
            {
                if (String.IsNullOrEmpty(number))
                {
                    break;
                }
                strData += ((char)Convert.ToInt32(number, 16)).ToString();
            }
            string[] lines = strData.Split(new[] { '\r', '\n', ',' });
            return lines;
        }


        static public bool DecodeBarcodeForIke(string serialString, out string hw, out string serial, out string build)
        {
            hw = string.Empty;
            serial = string.Empty;
            build = string.Empty;

            // get hardware version
            string pattern = @"^\d+\.\d+[A-Z]";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            Match match = Regex.Match(serialString, pattern, RegexOptions.IgnoreCase);
            if (match.Success) hw = match.Value;
            else return false;

            // get serial number
            string stmp = serialString.Substring(hw.Length);
            pattern = @"\d+[-]";
            rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            match = Regex.Match(stmp, pattern, RegexOptions.IgnoreCase);
            if (match.Success) serial = match.Value.Substring(0, match.Value.Length - 1);
            else return false;

            // get build number
            stmp = stmp.Substring(serial.Length + 1);
            pattern = @"\d+";
            rgx = new Regex(pattern, RegexOptions.IgnoreCase);
            match = Regex.Match(stmp, pattern, RegexOptions.IgnoreCase);
            if (match.Success) build = match.Value;
            else return false;

            return true;
        }


        public string extractHardwareRevision(string serialString)
        {
            string result = "";
            string pattern = @"^\d+\.\d+[A-Z]";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);

            Match match = Regex.Match(serialString, pattern, RegexOptions.IgnoreCase);
            if (match.Success) result = match.Value;
            else result = "";

            return result;
        }

        public string extractSerialNumber(string serialString)
        {
            string pattern = @"\d+[-]\d+";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);

            string result;
            //= Regex.Replace(serialString, pattern, string.Empty, RegexOptions.IgnoreCase);

            Match match = Regex.Match(serialString, pattern, RegexOptions.IgnoreCase);
            if (match.Success) result = match.Value;
            else result = "";

            return result;
        }

        public string extractBuildVersion(string serialString)
        {
            string pattern = @"[-]\d+";
            var rgx = new Regex(pattern, RegexOptions.IgnoreCase);

            string result = Regex.Replace(serialString, pattern, string.Empty, RegexOptions.IgnoreCase);

            return result.TrimStart('-');
        }



    }
}
