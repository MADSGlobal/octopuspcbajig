﻿namespace ProdTest
{
    partial class OperatorDialog
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OperatorDialog));
            this.txtEnterOperator = new System.Windows.Forms.TextBox();
            this.btnEnterOperator = new System.Windows.Forms.Button();
            this.lblEnterOperator = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtEnterOperator
            // 
            this.txtEnterOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEnterOperator.Location = new System.Drawing.Point(25, 57);
            this.txtEnterOperator.Name = "txtEnterOperator";
            this.txtEnterOperator.Size = new System.Drawing.Size(408, 26);
            this.txtEnterOperator.TabIndex = 0;
            this.txtEnterOperator.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtEnterOperator_KeyDown);
            // 
            // btnEnterOperator
            // 
            this.btnEnterOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnterOperator.Location = new System.Drawing.Point(186, 98);
            this.btnEnterOperator.Name = "btnEnterOperator";
            this.btnEnterOperator.Size = new System.Drawing.Size(75, 30);
            this.btnEnterOperator.TabIndex = 1;
            this.btnEnterOperator.Text = "OK";
            this.btnEnterOperator.UseVisualStyleBackColor = true;
            this.btnEnterOperator.Click += new System.EventHandler(this.btnEnterOperator_Click);
            // 
            // lblEnterOperator
            // 
            this.lblEnterOperator.AutoSize = true;
            this.lblEnterOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnterOperator.Location = new System.Drawing.Point(21, 19);
            this.lblEnterOperator.Name = "lblEnterOperator";
            this.lblEnterOperator.Size = new System.Drawing.Size(170, 20);
            this.lblEnterOperator.TabIndex = 2;
            this.lblEnterOperator.Text = "Enter Operator\'s name";
            // 
            // OperatorDialog
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(457, 146);
            this.ControlBox = false;
            this.Controls.Add(this.lblEnterOperator);
            this.Controls.Add(this.btnEnterOperator);
            this.Controls.Add(this.txtEnterOperator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OperatorDialog";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Operator dialog";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtEnterOperator;
        private System.Windows.Forms.Button btnEnterOperator;
        private System.Windows.Forms.Label lblEnterOperator;
    }
}