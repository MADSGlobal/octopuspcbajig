﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Windows;

namespace ProdTest
{
    public partial class OperatorDialog : Form
    {
        public string txtReturn;
        public string txtRequest
        {
            set {  lblEnterOperator.Text = value; }
        }

        public OperatorDialog()
        {
            InitializeComponent();
        }


        bool IsValid()
        {
            if (this.txtEnterOperator.Text == string.Empty) return false;
//            if (!(this.txtEnterOperator.Text.All(char.IsLetter))) return false;
            return true;
        }


        private void ProcessReturn()
        {
            if (IsValid())
            {
                txtReturn = this.txtEnterOperator.Text;
                this.DialogResult = DialogResult.OK;
            }
            this.txtEnterOperator.Text = string.Empty;
            this.txtEnterOperator.Focus();
        }


        private void btnEnterOperator_Click(object sender, EventArgs e)
        {
            ProcessReturn();
        }


        private void txtEnterOperator_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13) ProcessReturn();
        }


    }
}
