﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ProdTest
{
    public class Nmea
    {
        /// <summary>
        /// Generates an 8-bit NMEA checksum (XOR)
        /// </summary>
        /// <param name="s">input string</param>
        /// <returns>checksum</returns>
        public static byte GenerateChecksum(string s)
        {
            Encoding ASCII = Encoding.ASCII;
            byte cs = 0;

            byte[] byteArray = ASCII.GetBytes(s);
            foreach (byte b in byteArray)
            {
                cs ^= b;
            }
            return cs;

        }

        /// <summary>
        /// Adds NMEA start and end markers plus checksum
        /// </summary>
        /// <param name="s">input string</param>
        /// <returns>input string with checksum and markers</returns>
        public static string AddHeaderChecksum(string s)
        {
            byte b = Nmea.GenerateChecksum(s);

            return "$" + s + "*" + b.ToString("X2");
        }

        // Asabove but without leading '$'
        public static string AddHalveHeaderChecksum(string s)
        {
            byte b = Nmea.GenerateChecksum(s);

            return s + "*" + b.ToString("X2");
        }

        /// <summary>
        /// Does a checksum verification and strips start and end markers
        /// </summary>
        /// <param name="s">input string</param>
        /// <returns>input string without checksum and markers or empty if checksum was not correct</returns>
        public static string StripHeaderChecksum(string s)
        {
            if (s.Length < 4) return string.Empty;

            // Simply strip last 2 chars as checksum and use s between 1 and (length-3) as core 
            string sChecksum = s.Substring((s.Length - 2), 2);
            string sCore = s.Substring(1, (s.Length - 4));
            byte b = Nmea.GenerateChecksum(sCore);

            // string compare looks easier here
            if (b.ToString("X2") == sChecksum) return sCore;
            return string.Empty;
        }

        // As above but without leading '$'
        public static string StripHalveHeaderChecksum(string s)
        {
            // Simply strip last 2 chars as checksum and use s between 1 and (length-3) as core 
            string sChecksum = s.Substring((s.Length - 2), 2);
            string sCore = s.Substring(0, (s.Length - 3));
            byte b = Nmea.GenerateChecksum(sCore);

            // string compare looks easier here
            if (b.ToString("X2") == sChecksum) return sCore;
            return string.Empty;
        }

    }




}
