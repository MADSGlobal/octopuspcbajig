﻿namespace ProdTest
{
    partial class OperatorActionRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnEnterOperator = new System.Windows.Forms.Button();
            this.lblOperatorAction = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnEnterOperator
            // 
            this.btnEnterOperator.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.btnEnterOperator.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEnterOperator.Location = new System.Drawing.Point(109, 66);
            this.btnEnterOperator.Name = "btnEnterOperator";
            this.btnEnterOperator.Size = new System.Drawing.Size(75, 30);
            this.btnEnterOperator.TabIndex = 1;
            this.btnEnterOperator.Text = "Cancel";
            this.btnEnterOperator.UseVisualStyleBackColor = true;
            this.btnEnterOperator.Click += new System.EventHandler(this.btnEnterOperator_Click);
            // 
            // lblOperatorAction
            // 
            this.lblOperatorAction.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblOperatorAction.AutoSize = true;
            this.lblOperatorAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblOperatorAction.Location = new System.Drawing.Point(70, 22);
            this.lblOperatorAction.Name = "lblOperatorAction";
            this.lblOperatorAction.Size = new System.Drawing.Size(149, 20);
            this.lblOperatorAction.TabIndex = 2;
            this.lblOperatorAction.Text = "Press Power Button";
            // 
            // OperatorActionRequest
            // 
            this.AcceptButton = this.btnEnterOperator;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.btnEnterOperator;
            this.ClientSize = new System.Drawing.Size(301, 118);
            this.ControlBox = false;
            this.Controls.Add(this.lblOperatorAction);
            this.Controls.Add(this.btnEnterOperator);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OperatorActionRequest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Operator action request";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnEnterOperator;
        private System.Windows.Forms.Label lblOperatorAction;
    }
}