﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ivi.Visa.Interop;
using Generics.Result;
using Utilities;


namespace ProdTest
{
    public delegate void LogHandler(string s, int level);
    public delegate void DatabaseHandler(Daq daq);


    public class DaqMeter : Daq
    {
        public DaqMeter(string name, string address, int delay) : base(DeviceType.dmm, name, address, delay) { }
    }
    

    public class DaqPower : Daq
    {
        public DaqPower(string name, string address, int delay) : base(DeviceType.psu, name, address, delay) { }


        /// <summary>
        /// Select channel of PSU
        /// </summary>
        /// <param name="chan">channel to select: 1 or 2</param>
        /// <param name="pause">pause in ms after command</param>
        /// <returns>success</returns>
        public bool SelectChannel(int chan, int pause, int loglevel)
        {
            if ((chan < 1) || (chan > 2)) return false;

            string s = chan.ToString("d1");
            if (LogHandler != null) LogHandler("Selecting PSU channel " + s + " ... ", loglevel);

            bool res = (SendCommand("INST:SEL OUT" + s, pause, 0, "Selecting channel " + s));
            if (res) {
                if (LogHandler != null) LogHandler ("OK\r\n", loglevel);
                return true;
            }
            if (LogHandler != null) LogHandler ("FAILED\r\n", loglevel);
            return false;
        }

        //! Separate into SetVoltage() and SetVoltageCheck()
        //! Check for device type
        //! Create overload w/o pause and w/o umin + umax -> set to 5% 
        /// <summary>
        ///   Sets the PSU to a new voltage
        /// </summary>
        /// <param name="u"> Voltage to set </param>        
        /// <param name="i"> Current limit </param>        
        /// <param name="umin"> Lower boundary for output voltage </param>     
        /// <param name="umax"> Upper boundary for output voltage </param>     
        /// <param name="name"> Human-readable name of channel </param>     
        /// <param name="pause"> delay in ms _between_ setting voltage and checking it </param>     
        /// <returns> success flag </returns>
        public bool SetVoltage(float u, float i, float umin, float umax, string name, int pause)
        {
            string s;
            float f;
            string su = u.ToString("0.000");
            string si = i.ToString("0.000");
            string smin = umin.ToString("0.000");
            string smax = umax.ToString("0.000");

            if (LogHandler != null) LogHandler("Setting " + name + " to " + su + "V - (" + smin + "V min, " + smax + "V max) ... ", 3);

            new Delay(delay);
            s = "CURR " + si + ";VOLT " + su;

            if (!(Command(s).isOK)) return false;

            new Delay(delay + pause);
            s = "MEAS:VOLT?";
            if (!(Command(s).isOK)) return false;

            new Delay(delay);
            GenericResult<string> r = Result();
            if (!r.isOK) return false;
            s = r.result;

            f = Convert.ToSingle(s);
            s = f.ToString("0.000");
            _lastResult = s;
            unit = "V";
            _channelName = name;

            // Don't show results of names starting with "_" to screen
            if (name.StartsWith("_"))
            {
                name.Substring(1, name.Length - 1);
            }
            else
            {
                if (DatabaseHandler != null) DatabaseHandler(this);
            }

            if ((f > umax) || (f < umin))
            {
                if (LogHandler != null) LogHandler("actual = " + s + "V ..... \tFAILED\r\n", 3);
                return false;
            }
            if (LogHandler != null) LogHandler("actual = " + s + "V ..... \tOK\r\n", 3);
            return true;
        }

        //! Add print/output function
        //! Check for device type
        public bool EnableOutput(bool on, int pause)
        {
            string s = "OUTP ";
            if (on) s += "ON";
            else s += "OFF";
            new Delay(delay + pause);
            bool b = Command(s).isOK;
            if (b)
            {
                new Delay(delay);
                return true;
            }
            return false;
        }


        //! Separate into GetCurrent() and GetCurrentCheck()
        //! Create overload w/o pause and w/o imin + imax -> set to 5% 
        /// <summary>
        ///   Reads actual output current from PSU
        /// </summary>
        /// <param name="imin"> Lower boundary for output current </param>     
        /// <param name="imax"> Upper boundary for output current </param>     
        /// <param name="name"> Human-readable name of channel </param>     
        /// <param name="pause"> delay in ms _before_ issuing command to device </param>     
        /// <returns> success flag </returns>
        public bool GetCurrent(float imin, float imax, string name, int pause)
        {
            string s;
            float f;

            string smin = imin.ToString("0.000");
            string smax = imax.ToString("0.000");

            if (LogHandler != null) LogHandler("Reading current " + name + " - (" + imin + "A min, " + imax + "A max) ... ", 3);

            new Delay(delay);
            s = "MEAS:CURR?";
            if (!(Command(s).isOK)) return false;

            new Delay(delay + pause);
            GenericResult<string> r = Result();
            if (!r.isOK) return false;
            s = r.result;

            f = Convert.ToSingle(s);
            s = f.ToString("0.000");
            _lastResult = s;
            unit = "A";
            _channelName = name;

            // ignore names starting with "_"
            if (name.StartsWith("_"))
            {
                name.Substring(1, name.Length - 1);
            }
            else
            {
                if (DatabaseHandler != null) DatabaseHandler(this);
            }

            if ((f > imax) || (f < imin))
            {
                if (LogHandler != null) LogHandler("actual = " + s + "A ..... \tFAILED\r\n", 3);
                return false;
            }
            if (LogHandler != null) LogHandler("actual = " + s + "A ..... \tOK\r\n", 3);
            return true;
        }

    }







    public class Daq
    {
        //!! error handling
        private static Ivi.Visa.Interop.ResourceManager grm = new ResourceManager();
        private Ivi.Visa.Interop.FormattedIO488 ioDmm = new FormattedIO488();

        public enum DeviceType { dmm, psu }
        public bool isLogging = true;

        private const int TimeOut = 2000;                   // Communication timeout limit

        public readonly string StrErr = "ERROR";  
        
        protected string _deviceName = string.Empty;          // human-readable device name
        protected string _deviceAddress = string.Empty;        // VISA address string
        protected DeviceType _deviceType;
        protected string _lastResult = string.Empty;

//--------------
        public enum UnitType { volts, amps }

        protected string _channelName = string.Empty;
        
        private int _delay;                                // delay to be added each time, 200ms min for PSU!!!
        private LogHandler _LogHandler = null;             // Callback to show/log results
        private DatabaseHandler _DatabaseHandler = null;


        public string unit { get; protected set; }

        public string channelName {
            get { return _channelName; }
            set { value = _channelName; }
        }


        public Daq(DeviceType type, string name, string address, int delay)
        {
            _deviceType = type;
            _deviceName = name;
            _deviceAddress = address;
            _delay = delay;
        }

        public Daq(DeviceType type, string name, string address) : this(type, name, address, 200) { }


        public string lastResult
        {
            get { return _lastResult; }
        }

        public string deviceName
        {
            set { _deviceName = value; }
            get { return _deviceName; }
        }

        public int delay
        {
            set { _delay = value; }
            get { return _delay; }
        }

        public string Address
        {
            set { _deviceAddress = value; }
            get { return _deviceAddress; }
        }

        public LogHandler LogHandler
        {
            set { _LogHandler = value; }
            get { return _LogHandler; }
        }

        public DatabaseHandler DatabaseHandler
        {
            set { _DatabaseHandler = value; }
            get { return _DatabaseHandler; }
        }

        protected void SetLastResult(string s)
        {
            _lastResult = s;
            if (isLogging) new LogWrite(s);
        }


        /// <summary>
        /// Open device via Visa.IVI.Interop layer
        /// </summary>
        /// <returns></returns>
        public GenericResult Open()
        {
            string s = "Device: " + _deviceName + ", Interface: " + _deviceAddress;

            try
            {
                ioDmm.IO = (IMessage)grm.Open(_deviceAddress, AccessMode.NO_LOCK, TimeOut, "");
            }
            catch (SystemException ex)
            {
                string s1 = "Can't find or open " + s;
                SetLastResult(s1);
                ioDmm.IO = null;
                throw new ApplicationException(s1, ex);
            }
            SetLastResult("Success opening " + s);
            return new GenericResult(true);
        }


        /// <summary>
        /// Close device via Visa.IVI.Interop layer
        /// </summary>
        public void Close()
        {
            string s = "Device: " + _deviceName + ", Interface: " + _deviceAddress;

            if (ioDmm.IO != null)
            {
                ioDmm.IO.Close();
                ioDmm.IO = null;
            }
            SetLastResult("Success closing " + s);
        }

        
        /// <summary>
        /// Sends a low-level command to a device via Visa.IVI.Interop layer
        /// </summary>
        /// <param name="cmd">command string to send</param>
        /// <returns>success flag</returns>
        public GenericResult Command(string cmd)
        //!! Add: Read error out of device if something goes wrong 
        {
            string s = "Device: " + _deviceName + ", Interface: " + _deviceAddress + "Command: " + cmd;
            if (ioDmm.IO == null) Open();

            try 
            { 
                ioDmm.WriteString(cmd, true);
            }
            catch (SystemException)
            {
                try  //again
                {
                    ioDmm.WriteString(cmd, true);
                }
                catch (SystemException ex)
                {
                    string s1 = "Communication to device failed (writing), " + s;
                    SetLastResult(s1);
                    throw new ApplicationException(s1, ex);
                }
            }
            finally
            {
                new Delay(_delay);
            }
            return new GenericResult(true);
        }


        /// <summary>
        /// Gets a low-level read from a device via Visa.IVI.Interop layer
        /// </summary>
        /// <returns></returns>
        public GenericResult<string> Result()
        //! Add: Read error out of device if something goes wrong 
        {
            string s = "Device: " + _deviceName + ", Interface: " + _deviceAddress;
            if (ioDmm.IO == null) Open();

            string sRes;
            try
            {
                sRes = ioDmm.ReadString();
            }
            catch (SystemException ex)
            {
                string s1 = "Communication to device failed (reading), " + s;
                SetLastResult(s1);
                throw new ApplicationException(s1, ex);
            }
            finally
            {
                new Delay(_delay * 2);
            }
            return new GenericResult<string>(true, string.Empty, sRes);
        }


        /// <summary>
        /// Read ID information from VISA/IVI device
        /// </summary>
        /// <returns></returns>
        public string GetID()
        {
            var c = Command("*IDN?");
            if (!(c.isOK))
            {
                SetLastResult("Can't read ID string from device");
                return string.Empty;
            }

            return Result().result;
        }

        /// <summary>
        /// Read version information from VISA/IVI device
        /// </summary>
        /// <returns></returns>
        public string GetVersion()
        {
            if (Command("SYST:VERS?").isOK == false)
            {
                SetLastResult("Can't read version information from device");
                return String.Empty;
            }
            return Result().result;
        }



        /// <summary>
        /// Sending a generic command string to a DMM. No result is queried, must be done seperate if neccessary
        /// </summary>
        /// <param name="s"> command string </param>
        /// <param name="pause"> extra pause after command, in ms </param>
        /// <param name="loglevel"> how to log event </param>
        /// <param name="name"> name of command for logging </param>
        /// <returns> success flag</returns>
        public bool SendCommand(string s, int pause, int loglevel, string name)
        {
            if (_LogHandler != null) _LogHandler("Sending command " + name + " (" + s + ") ... ", loglevel);
            new Delay(_delay + pause);

            bool b = Command(s).isOK;
            if (b)
            {
                if (_LogHandler != null) _LogHandler("OK\r\n", loglevel);
                return true;
            }
            if (_LogHandler != null) _LogHandler("FAILED\r\n", loglevel);
            return false;
        }




        bool DmmRead(int chan, char unit, string name, int channelDelay, out float reading)
        {
            string s, sunit = "VOLT";
            float f;
            bool doLog = true;

            reading = 0;

            string schan = chan.ToString("000");
            if ((name == string.Empty) || (_LogHandler == null)) doLog = false;

            if (doLog) _LogHandler("Reading " + name + " - (Ch " + schan + ") ... ", 3);

            if (unit == 'V') sunit = "VOLT";
            if (unit == 'A') sunit = "CURR";

            s = "CONF:" + sunit + ":DC" + " (@" + schan + ")";
            new Delay(_delay);
            if (!(Command(s).isOK)) return false;

            string d = (((float)channelDelay) / 1000).ToString("0.000");
            s = "ROUT:CHAN:DELAY " + d + ", (@" + schan + ")";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            s = "READ?";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            new Delay(_delay);
            s = Result().result;
            if (s == StrErr) return false;

            f = Convert.ToSingle(s);
            s = f.ToString("0.000");
            _lastResult = s;
            _channelName = name;

            // ignore names starting with "_" for database
            if (doLog)
            {
                if (!name.StartsWith("_"))
                {
                    name.Substring(1, name.Length - 1);
                }
                else
                {
                    if (_DatabaseHandler != null) _DatabaseHandler(this);
                }
            }

            if (doLog) _LogHandler("actual = " + s + unit + " ..... \tOK\r\n", 3);
            reading = f;
            return true;
        }


        //!! Optimise the crude underscore way!
        /// <summary>
        ///  Reads a voltage or current from the DMM and compares with boundaries
        ///  if name starts with "_" it won't be logged
        ///  if name is empty it also won't be displayed
        /// </summary>
        /// <param name="chan"> channel to measure (101 to 140 on card 1, etc) </param>
        /// <param name="unit"> either 'V' for voltage or 'A' for current </param>
        /// <param name="vmin"> Lower acceptable boundary </param>
        /// <param name="vmax"> Upper acceptable boundary </param>
        /// <param name="name"> Human-readable name of channel, for logging and displaying </param>
        /// <returns> success flag </returns>
        bool DmmGet(int chan, char unit, float vmin, float vmax, string name, int channelDelay)
        {
            string s, sunit = "VOLT";
            float f;
            bool doLog = true;

            string smin = vmin.ToString("0.000");
            string smax = vmax.ToString("0.000");
            string schan = chan.ToString("000");
            if ((name == string.Empty) || (_LogHandler == null)) doLog = false;

            if (doLog) _LogHandler("Reading " + name + " - (Ch " + schan + ", " + smin + unit + " min, " + smax + unit + " max) ... ", 3);

            if (unit == 'V') sunit = "VOLT";
            if (unit == 'A') sunit = "CURR";

            s = "CONF:" + sunit + ":DC" + " (@" + schan + ")";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            string d = (((float) channelDelay) / 1000).ToString("0.000");
            s = "ROUT:CHAN:DELAY " + d + ", (@" + schan + ")";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            s = "READ?";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            new Delay(_delay);
            s = Result().result;
            if (s == StrErr) return false;

            f = Convert.ToSingle(s);
            s = f.ToString("0.000");
            _lastResult = s;
            _channelName = name;

            // ignore names starting with "_" for database
            if (doLog)
            {
                if (name.StartsWith("_"))
                {
                    name.Substring(1, name.Length - 1);
                }
                else
                {
                    if (_DatabaseHandler != null) _DatabaseHandler(this);
                }
            }

            if ((f > vmax) || (f < vmin))
            {
                if (doLog) _LogHandler("actual = " + s + unit + " ..... \tFAILED\r\n", 3);
                return false;
            }
            if (doLog) _LogHandler("actual = " + s + unit + " ..... \tOK\r\n", 3);
            return true;
        }

        /// <summary>
        /// Reads a voltage from the DMM
        /// </summary>
        /// <param name="chan">channel to measure (101 to 140 on card 1, etc)</param>
        /// <param name="name">Human-readable name of channel, for logging and displaying</param>
        /// <param name="reading">reading</param>
        /// <returns>success flag</returns>
        public bool ReadVoltage(int chan, string name, out float reading)
        {
            return DmmRead(chan, 'V', name, 5, out reading);
        }

        /// <summary>
        /// Reads a voltage from the DMM
        /// </summary>
        /// <param name="chan">channel to measure (101 to 140 on card 1, etc)</param>
        /// <param name="name">Human-readable name of channel, for logging and displaying</param>
        /// <param name="reading">reading</param>
        /// <returns>success flag</returns>
        public bool ReadVoltage(int chan, string name, int channeldelay, out float reading)
        {
            return DmmRead(chan, 'V', name, channeldelay, out reading);
        }

        //! use enum for type
        //! Separate into GetCurrent() and GetCurrentCheck()
        /// <summary>
        ///  Reads a current from the DMM and compares with boundaries. 5ms channel delay time
        /// </summary>
        /// <param name="chan"> channel to measure (101 to 140 on card 1, etc) </param>
        /// <param name="vmin"> Lower acceptable boundary </param>
        /// <param name="vmax"> Upper acceptable boundary </param>
        /// <param name="name"> Human-readable name of channel, for logging and displaying </param>
        /// <returns> success flag </returns>
        public bool GetCurrent(int chan, float imin, float imax, string name)
        {
            return DmmGet(chan, 'A', imin, imax, name, 5);
        }

        //! use enum for type
        //! Separate into GetVoltage() and GetVoltageCheck()
        /// <summary>
        ///  Reads a voltage from the DMM and compares with boundaries.
        /// </summary>
        /// <param name="chan"> channel to measure (101 to 140 on card 1, etc) </param>
        /// <param name="vmin"> Lower acceptable boundary </param>
        /// <param name="vmax"> Upper acceptable boundary </param>
        /// <param name="name"> Human-readable name of channel, for logging and displaying </param>
        /// <returns> success flag </returns>
        public bool GetVoltage(int chan, float vmin, float vmax, string name, int channelDelay)
        {
            return DmmGet(chan, 'V', vmin, vmax, name, channelDelay);
        }

        //! use enum for type
        //! Separate into GetVoltage() and GetVoltageCheck()
        /// <summary>
        ///  Reads a voltage from the DMM and compares with boundaries. 5ms channel delay time
        /// </summary>
        /// <param name="chan"> channel to measure (101 to 140 on card 1, etc) </param>
        /// <param name="vmin"> Lower acceptable boundary </param>
        /// <param name="vmax"> Upper acceptable boundary </param>
        /// <param name="name"> Human-readable name of channel, for logging and displaying </param>
        /// <returns> success flag </returns>
        public bool GetVoltage(int chan, float vmin, float vmax, string name)
        {
            return DmmGet(chan, 'V', vmin, vmax, name, 5);
        }


        //! Check for device type
        /// <summary>
        ///  Switches Relay card 
        /// </summary>
        /// <param name="chan"> channel to open/close </param>
        /// <param name="close"> close flag, otherwise open </param>
        /// <param name="pause"> pause in ms AFTER relay switch </param>
        /// <param name="name"> name of channel for logging/displaying </param>
        /// <returns> success flag </returns>
        bool SwitchRelais(int chan, bool close, int pause, string name)
        {
            string s, sopenclose;

            string schan = chan.ToString("000");
            if (close) sopenclose = "Clos";
            else sopenclose = "Open";
            if (!name.StartsWith("_"))
            {
                if (_LogHandler != null) _LogHandler(sopenclose + "ing relay: " + name + " - (" + schan + ") ... ",3);
            }
            s = "ROUT:" + sopenclose + " (@" + schan + ")";
            new Delay(_delay);
            if (!(this.Command(s).isOK)) return false;

            if (!name.StartsWith("_"))
            {
                if (_LogHandler != null) _LogHandler("OK\r\n", 3);
            }
            return true;

            // Check not needed
            s = "ROUT:CLOS? (@" + schan + ")";
            new Delay(_delay + pause);
            if (!(this.Command(s).isOK)) return false;

            new Delay(_delay);
            s = Result().result;
            if (((s[0] == '1') && close) || ((s[0] == '0') && (!close)))
            {
                 if (_LogHandler != null) _LogHandler ("OK\r\n", 3);
                return true;
            }
             if (_LogHandler != null) _LogHandler("FAILED\r\n", 3);
            return false;

        }

        //! Check for device type
        /// <summary>
        ///  Closes relay on relais card
        /// </summary>
        /// <param name="chan"> channel to open/close </param>
        /// <param name="pause"> pause in ms AFTER relay switch </param>
        /// <param name="name"> name of channel for logging/displaying </param>
        /// <returns> success flag </returns>
        public bool RelaisClose(int chan, int pause, string name)
        {
            return SwitchRelais(chan, true, pause, name);
        }

        //! Check for device type
        /// <summary>
        ///  Opens relay on relais card
        /// </summary>
        /// <param name="chan"> channel to open/close </param>
        /// <param name="pause">pause in ms AFTER relay switch</param>
        /// <param name="name"> name of channel for logging/displaying </param>
        /// <returns> success flag </returns>
        public bool RelaisOpen(int chan, int pause, string name)
        {
            return SwitchRelais(chan, false, pause, name);
        }

        /// <summary>
        /// Reset a card
        /// </summary>
        /// <param name="chan">channel on card to reset, usually 100, 200 or 300</param>
        /// <param name="pause">pause in ms AFTER relay switch</param>
        /// <returns></returns>
        public bool CardReset(int chan, int pause)
        {
            return (SendCommand("SYST:CPON " + chan.ToString(), pause, 0, "Card Reset"));
        }

    }
}
