﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Xml;
using CouchDbAPI;
using Ivi.Visa.Interop;
using ProdTest.Properties;
using CoreScanner;

namespace ProdTest
{
    public partial class ProdTest : Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ProdTest));
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.btnInitMeter1 = new System.Windows.Forms.Button();
            this.btnInitMeter2 = new System.Windows.Forms.Button();
            this.txtUnitSerial = new System.Windows.Forms.TextBox();
            this.txtUnitGps = new System.Windows.Forms.TextBox();
            this.txtUnitCell = new System.Windows.Forms.TextBox();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabMainRun = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtOut = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblUnitCell = new System.Windows.Forms.Label();
            this.lblUnitGps = new System.Windows.Forms.Label();
            this.lblMain = new System.Windows.Forms.Label();
            this.lblUnitSerial = new System.Windows.Forms.Label();
            this.btnStart = new System.Windows.Forms.Button();
            this.grpOptions = new System.Windows.Forms.GroupBox();
            this.chkProgramFirmware = new System.Windows.Forms.CheckBox();
            this.tabMainConfig = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPageSetupGeneric = new System.Windows.Forms.TabPage();
            this.chkTestingLisa = new System.Windows.Forms.CheckBox();
            this.lblReplicaDatabaseAddress = new System.Windows.Forms.Label();
            this.chkMustHaveCell = new System.Windows.Forms.CheckBox();
            this.chkMustHaveGps = new System.Windows.Forms.CheckBox();
            this.lblDutComPort = new System.Windows.Forms.Label();
            this.txtDutComPort = new System.Windows.Forms.TextBox();
            this.lblSetupOperator = new System.Windows.Forms.Label();
            this.txtSetupOperator = new System.Windows.Forms.TextBox();
            this.lblCouchDatabaseAddress = new System.Windows.Forms.Label();
            this.txtCouchDatabaseAddress = new System.Windows.Forms.TextBox();
            this.txtSetupProdname = new System.Windows.Forms.TextBox();
            this.lblSetupProdname = new System.Windows.Forms.Label();
            this.tabPageSetupDaq = new System.Windows.Forms.TabPage();
            this.txtAddress1 = new System.Windows.Forms.TextBox();
            this.txtIDStr1 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPageSetupPsu = new System.Windows.Forms.TabPage();
            this.txtAddress2 = new System.Windows.Forms.TextBox();
            this.txtIDStr2 = new System.Windows.Forms.TextBox();
            this.lblAddress = new System.Windows.Forms.Label();
            this.lblIDString = new System.Windows.Forms.Label();
            this.tabPageSetupProgrammers = new System.Windows.Forms.TabPage();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnControllerApplication = new System.Windows.Forms.Button();
            this.lblControllerApplication = new System.Windows.Forms.Label();
            this.txtControllerApplication = new System.Windows.Forms.TextBox();
            this.lblControllerProgrammer = new System.Windows.Forms.Label();
            this.btnControllerProgrammer = new System.Windows.Forms.Button();
            this.txtControllerProgrammer = new System.Windows.Forms.TextBox();
            this.btnSaveSettings = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.statusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.DUTSerialPort = new System.IO.Ports.SerialPort(this.components);
            this.backgroundGetPowerbutton = new System.ComponentModel.BackgroundWorker();
            this.LisaSerialPort = new System.IO.Ports.SerialPort(this.components);
            this.tabControl1.SuspendLayout();
            this.tabMainRun.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.grpOptions.SuspendLayout();
            this.tabMainConfig.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPageSetupGeneric.SuspendLayout();
            this.tabPageSetupDaq.SuspendLayout();
            this.tabPageSetupPsu.SuspendLayout();
            this.tabPageSetupProgrammers.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnInitMeter1
            // 
            this.btnInitMeter1.Location = new System.Drawing.Point(610, 10);
            this.btnInitMeter1.Name = "btnInitMeter1";
            this.btnInitMeter1.Size = new System.Drawing.Size(104, 30);
            this.btnInitMeter1.TabIndex = 49;
            this.btnInitMeter1.Text = "Test";
            this.toolTip1.SetToolTip(this.btnInitMeter1, "Click to initialize the IO enviornment");
            this.btnInitMeter1.Click += new System.EventHandler(this.btnInitMeter1_Click);
            // 
            // btnInitMeter2
            // 
            this.btnInitMeter2.Location = new System.Drawing.Point(610, 10);
            this.btnInitMeter2.Name = "btnInitMeter2";
            this.btnInitMeter2.Size = new System.Drawing.Size(104, 30);
            this.btnInitMeter2.TabIndex = 26;
            this.btnInitMeter2.Text = "Test";
            this.toolTip1.SetToolTip(this.btnInitMeter2, "Click to initialize the IO enviornment");
            this.btnInitMeter2.Click += new System.EventHandler(this.btnInitMeter2_Click);
            // 
            // txtUnitSerial
            // 
            this.txtUnitSerial.Location = new System.Drawing.Point(118, 22);
            this.txtUnitSerial.Name = "txtUnitSerial";
            this.txtUnitSerial.Size = new System.Drawing.Size(183, 26);
            this.txtUnitSerial.TabIndex = 0;
            this.toolTip1.SetToolTip(this.txtUnitSerial, "Enter or scan serial number into here");
            this.txtUnitSerial.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtUnitSerial_KeyDown);
            // 
            // txtUnitGps
            // 
            this.txtUnitGps.Location = new System.Drawing.Point(118, 66);
            this.txtUnitGps.Name = "txtUnitGps";
            this.txtUnitGps.Size = new System.Drawing.Size(183, 26);
            this.txtUnitGps.TabIndex = 15;
            this.toolTip1.SetToolTip(this.txtUnitGps, "Enter or scan serial number into here");
            // 
            // txtUnitCell
            // 
            this.txtUnitCell.Location = new System.Drawing.Point(118, 109);
            this.txtUnitCell.Name = "txtUnitCell";
            this.txtUnitCell.Size = new System.Drawing.Size(183, 26);
            this.txtUnitCell.TabIndex = 16;
            this.toolTip1.SetToolTip(this.txtUnitCell, "Enter or scan serial number into here");
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabMainRun);
            this.tabControl1.Controls.Add(this.tabMainConfig);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1184, 692);
            this.tabControl1.TabIndex = 21;
            this.tabControl1.Selecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Selecting);
            this.tabControl1.Deselecting += new System.Windows.Forms.TabControlCancelEventHandler(this.tabControl1_Deselecting);
            // 
            // tabMainRun
            // 
            this.tabMainRun.Controls.Add(this.tableLayoutPanel1);
            this.tabMainRun.Location = new System.Drawing.Point(4, 29);
            this.tabMainRun.Margin = new System.Windows.Forms.Padding(0);
            this.tabMainRun.Name = "tabMainRun";
            this.tabMainRun.Size = new System.Drawing.Size(1176, 659);
            this.tabMainRun.TabIndex = 2;
            this.tabMainRun.Text = "Test Run";
            this.tabMainRun.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 600F));
            this.tableLayoutPanel1.Controls.Add(this.txtOut, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 215F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1176, 659);
            this.tableLayoutPanel1.TabIndex = 16;
            // 
            // txtOut
            // 
            this.txtOut.BackColor = System.Drawing.SystemColors.Window;
            this.tableLayoutPanel1.SetColumnSpan(this.txtOut, 3);
            this.txtOut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtOut.Location = new System.Drawing.Point(0, 215);
            this.txtOut.Margin = new System.Windows.Forms.Padding(0);
            this.txtOut.MaxLength = 2000000;
            this.txtOut.Multiline = true;
            this.txtOut.Name = "txtOut";
            this.txtOut.ReadOnly = true;
            this.txtOut.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOut.Size = new System.Drawing.Size(1176, 444);
            this.txtOut.TabIndex = 5;
            // 
            // panel1
            // 
            this.panel1.AutoSize = true;
            this.panel1.Controls.Add(this.lblUnitCell);
            this.panel1.Controls.Add(this.lblUnitGps);
            this.panel1.Controls.Add(this.txtUnitCell);
            this.panel1.Controls.Add(this.txtUnitGps);
            this.panel1.Controls.Add(this.lblMain);
            this.panel1.Controls.Add(this.lblUnitSerial);
            this.panel1.Controls.Add(this.txtUnitSerial);
            this.panel1.Controls.Add(this.btnStart);
            this.panel1.Controls.Add(this.grpOptions);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(579, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(594, 209);
            this.panel1.TabIndex = 16;
            // 
            // lblUnitCell
            // 
            this.lblUnitCell.AutoSize = true;
            this.lblUnitCell.Location = new System.Drawing.Point(21, 112);
            this.lblUnitCell.Name = "lblUnitCell";
            this.lblUnitCell.Size = new System.Drawing.Size(91, 20);
            this.lblUnitCell.TabIndex = 18;
            this.lblUnitCell.Text = "Cell PCBA#";
            // 
            // lblUnitGps
            // 
            this.lblUnitGps.AutoSize = true;
            this.lblUnitGps.Location = new System.Drawing.Point(13, 69);
            this.lblUnitGps.Name = "lblUnitGps";
            this.lblUnitGps.Size = new System.Drawing.Size(99, 20);
            this.lblUnitGps.TabIndex = 17;
            this.lblUnitGps.Text = "GPS PCBA#";
            // 
            // lblMain
            // 
            this.lblMain.AutoSize = true;
            this.lblMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblMain.Location = new System.Drawing.Point(129, 160);
            this.lblMain.Name = "lblMain";
            this.lblMain.Size = new System.Drawing.Size(162, 22);
            this.lblMain.TabIndex = 10;
            this.lblMain.Text = "Main PCBA detection";
            // 
            // lblUnitSerial
            // 
            this.lblUnitSerial.AutoSize = true;
            this.lblUnitSerial.Location = new System.Drawing.Point(13, 25);
            this.lblUnitSerial.Name = "lblUnitSerial";
            this.lblUnitSerial.Size = new System.Drawing.Size(99, 20);
            this.lblUnitSerial.TabIndex = 2;
            this.lblUnitSerial.Text = "Main PCBA#";
            // 
            // btnStart
            // 
            this.btnStart.Enabled = false;
            this.btnStart.Location = new System.Drawing.Point(321, 95);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(257, 96);
            this.btnStart.TabIndex = 1;
            this.btnStart.Text = "Start";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // grpOptions
            // 
            this.grpOptions.Controls.Add(this.chkProgramFirmware);
            this.grpOptions.Location = new System.Drawing.Point(321, 13);
            this.grpOptions.Name = "grpOptions";
            this.grpOptions.Size = new System.Drawing.Size(257, 65);
            this.grpOptions.TabIndex = 6;
            this.grpOptions.TabStop = false;
            this.grpOptions.Text = "Options";
            // 
            // chkProgramFirmware
            // 
            this.chkProgramFirmware.AutoSize = true;
            this.chkProgramFirmware.Checked = true;
            this.chkProgramFirmware.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkProgramFirmware.Location = new System.Drawing.Point(74, 25);
            this.chkProgramFirmware.Name = "chkProgramFirmware";
            this.chkProgramFirmware.Size = new System.Drawing.Size(151, 24);
            this.chkProgramFirmware.TabIndex = 23;
            this.chkProgramFirmware.Text = "program firmware";
            this.chkProgramFirmware.UseVisualStyleBackColor = true;
            // 
            // tabMainConfig
            // 
            this.tabMainConfig.Controls.Add(this.tabControl2);
            this.tabMainConfig.Controls.Add(this.btnSaveSettings);
            this.tabMainConfig.Location = new System.Drawing.Point(4, 29);
            this.tabMainConfig.Name = "tabMainConfig";
            this.tabMainConfig.Padding = new System.Windows.Forms.Padding(3);
            this.tabMainConfig.Size = new System.Drawing.Size(1176, 659);
            this.tabMainConfig.TabIndex = 3;
            this.tabMainConfig.Text = "Setup";
            this.tabMainConfig.UseVisualStyleBackColor = true;
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPageSetupGeneric);
            this.tabControl2.Controls.Add(this.tabPageSetupDaq);
            this.tabControl2.Controls.Add(this.tabPageSetupPsu);
            this.tabControl2.Controls.Add(this.tabPageSetupProgrammers);
            this.tabControl2.Dock = System.Windows.Forms.DockStyle.Top;
            this.tabControl2.Location = new System.Drawing.Point(3, 3);
            this.tabControl2.Margin = new System.Windows.Forms.Padding(5);
            this.tabControl2.Multiline = true;
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(1170, 587);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPageSetupGeneric
            // 
            this.tabPageSetupGeneric.Controls.Add(this.chkTestingLisa);
            this.tabPageSetupGeneric.Controls.Add(this.lblReplicaDatabaseAddress);
            this.tabPageSetupGeneric.Controls.Add(this.chkMustHaveCell);
            this.tabPageSetupGeneric.Controls.Add(this.chkMustHaveGps);
            this.tabPageSetupGeneric.Controls.Add(this.lblDutComPort);
            this.tabPageSetupGeneric.Controls.Add(this.txtDutComPort);
            this.tabPageSetupGeneric.Controls.Add(this.lblSetupOperator);
            this.tabPageSetupGeneric.Controls.Add(this.txtSetupOperator);
            this.tabPageSetupGeneric.Controls.Add(this.lblCouchDatabaseAddress);
            this.tabPageSetupGeneric.Controls.Add(this.txtCouchDatabaseAddress);
            this.tabPageSetupGeneric.Controls.Add(this.txtSetupProdname);
            this.tabPageSetupGeneric.Controls.Add(this.lblSetupProdname);
            this.tabPageSetupGeneric.Location = new System.Drawing.Point(4, 29);
            this.tabPageSetupGeneric.Name = "tabPageSetupGeneric";
            this.tabPageSetupGeneric.Size = new System.Drawing.Size(1162, 554);
            this.tabPageSetupGeneric.TabIndex = 2;
            this.tabPageSetupGeneric.Text = "Generic";
            this.tabPageSetupGeneric.UseVisualStyleBackColor = true;
            // 
            // chkTestingLisa
            // 
            this.chkTestingLisa.AutoSize = true;
            this.chkTestingLisa.Checked = true;
            this.chkTestingLisa.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestingLisa.Location = new System.Drawing.Point(271, 288);
            this.chkTestingLisa.Name = "chkTestingLisa";
            this.chkTestingLisa.Size = new System.Drawing.Size(274, 24);
            this.chkTestingLisa.TabIndex = 13;
            this.chkTestingLisa.Text = "Including cell phone module (LISA)";
            this.chkTestingLisa.UseVisualStyleBackColor = true;
            this.chkTestingLisa.CheckedChanged += new System.EventHandler(this.chkTestingLisa_CheckedChanged);
            // 
            // lblReplicaDatabaseAddress
            // 
            this.lblReplicaDatabaseAddress.AutoSize = true;
            this.lblReplicaDatabaseAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblReplicaDatabaseAddress.Location = new System.Drawing.Point(268, 135);
            this.lblReplicaDatabaseAddress.Name = "lblReplicaDatabaseAddress";
            this.lblReplicaDatabaseAddress.Size = new System.Drawing.Size(395, 16);
            this.lblReplicaDatabaseAddress.TabIndex = 12;
            this.lblReplicaDatabaseAddress.Text = "Use \"http://localhost:5984/ikedata\" for replication to remote server";
            // 
            // chkMustHaveCell
            // 
            this.chkMustHaveCell.AutoSize = true;
            this.chkMustHaveCell.Checked = true;
            this.chkMustHaveCell.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMustHaveCell.Location = new System.Drawing.Point(301, 318);
            this.chkMustHaveCell.Name = "chkMustHaveCell";
            this.chkMustHaveCell.Size = new System.Drawing.Size(408, 24);
            this.chkMustHaveCell.TabIndex = 10;
            this.chkMustHaveCell.Text = "A PCB number for cell phone module must be entered";
            this.chkMustHaveCell.UseVisualStyleBackColor = true;
            // 
            // chkMustHaveGps
            // 
            this.chkMustHaveGps.AutoSize = true;
            this.chkMustHaveGps.Checked = true;
            this.chkMustHaveGps.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkMustHaveGps.Location = new System.Drawing.Point(271, 258);
            this.chkMustHaveGps.Name = "chkMustHaveGps";
            this.chkMustHaveGps.Size = new System.Drawing.Size(370, 24);
            this.chkMustHaveGps.TabIndex = 9;
            this.chkMustHaveGps.Text = "A PCB number for GPS module must be entered";
            this.chkMustHaveGps.UseVisualStyleBackColor = true;
            // 
            // lblDutComPort
            // 
            this.lblDutComPort.AutoSize = true;
            this.lblDutComPort.Location = new System.Drawing.Point(136, 194);
            this.lblDutComPort.Name = "lblDutComPort";
            this.lblDutComPort.Size = new System.Drawing.Size(129, 20);
            this.lblDutComPort.TabIndex = 8;
            this.lblDutComPort.Text = "Device COM port";
            // 
            // txtDutComPort
            // 
            this.txtDutComPort.Location = new System.Drawing.Point(271, 191);
            this.txtDutComPort.Name = "txtDutComPort";
            this.txtDutComPort.Size = new System.Drawing.Size(438, 26);
            this.txtDutComPort.TabIndex = 7;
            // 
            // lblSetupOperator
            // 
            this.lblSetupOperator.AutoSize = true;
            this.lblSetupOperator.Location = new System.Drawing.Point(193, 53);
            this.lblSetupOperator.Name = "lblSetupOperator";
            this.lblSetupOperator.Size = new System.Drawing.Size(72, 20);
            this.lblSetupOperator.TabIndex = 6;
            this.lblSetupOperator.Text = "Operator";
            // 
            // txtSetupOperator
            // 
            this.txtSetupOperator.Location = new System.Drawing.Point(271, 50);
            this.txtSetupOperator.Name = "txtSetupOperator";
            this.txtSetupOperator.Size = new System.Drawing.Size(438, 26);
            this.txtSetupOperator.TabIndex = 5;
            // 
            // lblCouchDatabaseAddress
            // 
            this.lblCouchDatabaseAddress.AutoSize = true;
            this.lblCouchDatabaseAddress.Location = new System.Drawing.Point(73, 109);
            this.lblCouchDatabaseAddress.Name = "lblCouchDatabaseAddress";
            this.lblCouchDatabaseAddress.Size = new System.Drawing.Size(192, 20);
            this.lblCouchDatabaseAddress.TabIndex = 4;
            this.lblCouchDatabaseAddress.Text = "Couch Database Address";
            // 
            // txtCouchDatabaseAddress
            // 
            this.txtCouchDatabaseAddress.Location = new System.Drawing.Point(271, 106);
            this.txtCouchDatabaseAddress.Name = "txtCouchDatabaseAddress";
            this.txtCouchDatabaseAddress.Size = new System.Drawing.Size(438, 26);
            this.txtCouchDatabaseAddress.TabIndex = 3;
            // 
            // txtSetupProdname
            // 
            this.txtSetupProdname.Location = new System.Drawing.Point(271, 12);
            this.txtSetupProdname.Name = "txtSetupProdname";
            this.txtSetupProdname.Size = new System.Drawing.Size(438, 26);
            this.txtSetupProdname.TabIndex = 1;
            // 
            // lblSetupProdname
            // 
            this.lblSetupProdname.AutoSize = true;
            this.lblSetupProdname.Location = new System.Drawing.Point(155, 15);
            this.lblSetupProdname.Name = "lblSetupProdname";
            this.lblSetupProdname.Size = new System.Drawing.Size(110, 20);
            this.lblSetupProdname.TabIndex = 0;
            this.lblSetupProdname.Text = "Product Name";
            // 
            // tabPageSetupDaq
            // 
            this.tabPageSetupDaq.Controls.Add(this.btnInitMeter1);
            this.tabPageSetupDaq.Controls.Add(this.txtAddress1);
            this.tabPageSetupDaq.Controls.Add(this.txtIDStr1);
            this.tabPageSetupDaq.Controls.Add(this.label3);
            this.tabPageSetupDaq.Controls.Add(this.label4);
            this.tabPageSetupDaq.Location = new System.Drawing.Point(4, 22);
            this.tabPageSetupDaq.Name = "tabPageSetupDaq";
            this.tabPageSetupDaq.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSetupDaq.Size = new System.Drawing.Size(1162, 561);
            this.tabPageSetupDaq.TabIndex = 1;
            this.tabPageSetupDaq.Text = "DAQ Device";
            this.tabPageSetupDaq.UseVisualStyleBackColor = true;
            // 
            // txtAddress1
            // 
            this.txtAddress1.Location = new System.Drawing.Point(219, 12);
            this.txtAddress1.Name = "txtAddress1";
            this.txtAddress1.Size = new System.Drawing.Size(385, 26);
            this.txtAddress1.TabIndex = 42;
            this.txtAddress1.Text = "USB0::0x0957::0x2007::MY49011321::0::INSTR";
            // 
            // txtIDStr1
            // 
            this.txtIDStr1.Location = new System.Drawing.Point(219, 50);
            this.txtIDStr1.Name = "txtIDStr1";
            this.txtIDStr1.ReadOnly = true;
            this.txtIDStr1.Size = new System.Drawing.Size(385, 26);
            this.txtIDStr1.TabIndex = 43;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(125, 15);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(88, 16);
            this.label3.TabIndex = 46;
            this.label3.Text = "Address";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(96, 53);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(117, 19);
            this.label4.TabIndex = 47;
            this.label4.Text = "Identification";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPageSetupPsu
            // 
            this.tabPageSetupPsu.Controls.Add(this.btnInitMeter2);
            this.tabPageSetupPsu.Controls.Add(this.txtAddress2);
            this.tabPageSetupPsu.Controls.Add(this.txtIDStr2);
            this.tabPageSetupPsu.Controls.Add(this.lblAddress);
            this.tabPageSetupPsu.Controls.Add(this.lblIDString);
            this.tabPageSetupPsu.Location = new System.Drawing.Point(4, 22);
            this.tabPageSetupPsu.Name = "tabPageSetupPsu";
            this.tabPageSetupPsu.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSetupPsu.Size = new System.Drawing.Size(1162, 561);
            this.tabPageSetupPsu.TabIndex = 3;
            this.tabPageSetupPsu.Text = "Power Supply Device";
            this.tabPageSetupPsu.UseVisualStyleBackColor = true;
            // 
            // txtAddress2
            // 
            this.txtAddress2.Location = new System.Drawing.Point(219, 12);
            this.txtAddress2.Name = "txtAddress2";
            this.txtAddress2.Size = new System.Drawing.Size(385, 26);
            this.txtAddress2.TabIndex = 21;
            this.txtAddress2.Text = "ASRL20::INSTR";
            // 
            // txtIDStr2
            // 
            this.txtIDStr2.Location = new System.Drawing.Point(219, 50);
            this.txtIDStr2.Name = "txtIDStr2";
            this.txtIDStr2.ReadOnly = true;
            this.txtIDStr2.Size = new System.Drawing.Size(385, 26);
            this.txtIDStr2.TabIndex = 22;
            // 
            // lblAddress
            // 
            this.lblAddress.Location = new System.Drawing.Point(125, 15);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Size = new System.Drawing.Size(88, 16);
            this.lblAddress.TabIndex = 24;
            this.lblAddress.Text = "Address";
            this.lblAddress.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // lblIDString
            // 
            this.lblIDString.Location = new System.Drawing.Point(98, 53);
            this.lblIDString.Name = "lblIDString";
            this.lblIDString.Size = new System.Drawing.Size(115, 21);
            this.lblIDString.TabIndex = 25;
            this.lblIDString.Text = "Identification";
            this.lblIDString.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPageSetupProgrammers
            // 
            this.tabPageSetupProgrammers.Controls.Add(this.groupBox2);
            this.tabPageSetupProgrammers.Location = new System.Drawing.Point(4, 22);
            this.tabPageSetupProgrammers.Name = "tabPageSetupProgrammers";
            this.tabPageSetupProgrammers.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageSetupProgrammers.Size = new System.Drawing.Size(1162, 561);
            this.tabPageSetupProgrammers.TabIndex = 4;
            this.tabPageSetupProgrammers.Text = "Programmer";
            this.tabPageSetupProgrammers.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnControllerApplication);
            this.groupBox2.Controls.Add(this.lblControllerApplication);
            this.groupBox2.Controls.Add(this.txtControllerApplication);
            this.groupBox2.Controls.Add(this.lblControllerProgrammer);
            this.groupBox2.Controls.Add(this.btnControllerProgrammer);
            this.groupBox2.Controls.Add(this.txtControllerProgrammer);
            this.groupBox2.Location = new System.Drawing.Point(83, 21);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(803, 112);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Controller Programmer";
            // 
            // btnControllerApplication
            // 
            this.btnControllerApplication.Location = new System.Drawing.Point(757, 63);
            this.btnControllerApplication.Name = "btnControllerApplication";
            this.btnControllerApplication.Size = new System.Drawing.Size(30, 30);
            this.btnControllerApplication.TabIndex = 9;
            this.btnControllerApplication.Text = "...";
            this.btnControllerApplication.UseVisualStyleBackColor = true;
            this.btnControllerApplication.Click += new System.EventHandler(this.btnControllerApplication_Click);
            // 
            // lblControllerApplication
            // 
            this.lblControllerApplication.AutoSize = true;
            this.lblControllerApplication.Location = new System.Drawing.Point(85, 68);
            this.lblControllerApplication.Name = "lblControllerApplication";
            this.lblControllerApplication.Size = new System.Drawing.Size(146, 20);
            this.lblControllerApplication.TabIndex = 11;
            this.lblControllerApplication.Text = "Application location";
            // 
            // txtControllerApplication
            // 
            this.txtControllerApplication.Location = new System.Drawing.Point(237, 65);
            this.txtControllerApplication.Name = "txtControllerApplication";
            this.txtControllerApplication.Size = new System.Drawing.Size(514, 26);
            this.txtControllerApplication.TabIndex = 10;
            // 
            // lblControllerProgrammer
            // 
            this.lblControllerProgrammer.AutoSize = true;
            this.lblControllerProgrammer.Location = new System.Drawing.Point(103, 30);
            this.lblControllerProgrammer.Name = "lblControllerProgrammer";
            this.lblControllerProgrammer.Size = new System.Drawing.Size(128, 20);
            this.lblControllerProgrammer.TabIndex = 5;
            this.lblControllerProgrammer.Text = "Program location";
            // 
            // btnControllerProgrammer
            // 
            this.btnControllerProgrammer.Location = new System.Drawing.Point(757, 25);
            this.btnControllerProgrammer.Name = "btnControllerProgrammer";
            this.btnControllerProgrammer.Size = new System.Drawing.Size(30, 30);
            this.btnControllerProgrammer.TabIndex = 4;
            this.btnControllerProgrammer.Text = "...";
            this.btnControllerProgrammer.UseVisualStyleBackColor = true;
            this.btnControllerProgrammer.Click += new System.EventHandler(this.btnControllerProgrammer_Click);
            // 
            // txtControllerProgrammer
            // 
            this.txtControllerProgrammer.Location = new System.Drawing.Point(237, 27);
            this.txtControllerProgrammer.Name = "txtControllerProgrammer";
            this.txtControllerProgrammer.Size = new System.Drawing.Size(514, 26);
            this.txtControllerProgrammer.TabIndex = 3;
            // 
            // btnSaveSettings
            // 
            this.btnSaveSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveSettings.Location = new System.Drawing.Point(3, 598);
            this.btnSaveSettings.Name = "btnSaveSettings";
            this.btnSaveSettings.Size = new System.Drawing.Size(1165, 40);
            this.btnSaveSettings.TabIndex = 2;
            this.btnSaveSettings.Text = "Save Settings";
            this.btnSaveSettings.UseVisualStyleBackColor = true;
            this.btnSaveSettings.Click += new System.EventHandler(this.btnSaveSettings_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.statusLabel});
            this.statusStrip1.Location = new System.Drawing.Point(0, 670);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1184, 22);
            this.statusStrip1.TabIndex = 22;
            // 
            // statusLabel
            // 
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(39, 17);
            this.statusLabel.Text = "Status";
            // 
            // DUTSerialPort
            // 
            this.DUTSerialPort.PortName = "COM21";
            this.DUTSerialPort.ReadTimeout = 2000;
            // 
            // backgroundGetPowerbutton
            // 
            this.backgroundGetPowerbutton.WorkerSupportsCancellation = true;
            this.backgroundGetPowerbutton.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundGetPowerbutton_DoWork);
            this.backgroundGetPowerbutton.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundGetPowerbutton_RunWorkerCompleted);
            // 
            // LisaSerialPort
            // 
            this.LisaSerialPort.ReadTimeout = 2000;
            // 
            // ProdTest
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1184, 692);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximumSize = new System.Drawing.Size(10000, 10000);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1000, 726);
            this.Name = "ProdTest";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Popeye Production Test";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ProdTest_FormClosed);
            this.Load += new System.EventHandler(this.ProdTest_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabMainRun.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.grpOptions.ResumeLayout(false);
            this.grpOptions.PerformLayout();
            this.tabMainConfig.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPageSetupGeneric.ResumeLayout(false);
            this.tabPageSetupGeneric.PerformLayout();
            this.tabPageSetupDaq.ResumeLayout(false);
            this.tabPageSetupDaq.PerformLayout();
            this.tabPageSetupPsu.ResumeLayout(false);
            this.tabPageSetupPsu.PerformLayout();
            this.tabPageSetupProgrammers.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion


        private ToolTip toolTip1;
        private TabControl tabControl1;
        private TabPage tabMainRun;
        private StatusStrip statusStrip1;
        private SerialPort DUTSerialPort;
        private TabPage tabMainConfig;
        private TabControl tabControl2;
        private TabPage tabPageSetupDaq;
        private Button btnInitMeter1;
        private TextBox txtAddress1;
        private TextBox txtIDStr1;
        private Label label3;
        private Label label4;
        private TabPage tabPageSetupGeneric;
        private TabPage tabPageSetupPsu;
        private Button btnInitMeter2;
        private TextBox txtAddress2;
        private TextBox txtIDStr2;
        private Label lblAddress;
        private Label lblIDString;
        private Button btnSaveSettings;
        private TextBox txtSetupProdname;
        private Label lblSetupProdname;
        private Label lblCouchDatabaseAddress;
        private TextBox txtCouchDatabaseAddress;
        private Label lblSetupOperator;
        private TextBox txtSetupOperator;
        private TabPage tabPageSetupProgrammers;
        private BackgroundWorker backgroundGetPowerbutton;
        private Label lblDutComPort;
        private TextBox txtDutComPort;
        private GroupBox groupBox2;
        private Label lblControllerProgrammer;
        private Button btnControllerProgrammer;
        private TextBox txtControllerProgrammer;
        private Button btnControllerApplication;
        private Label lblControllerApplication;
        private TextBox txtControllerApplication;
        private SerialPort LisaSerialPort;
        private ToolStripStatusLabel statusLabel;
        private CheckBox chkMustHaveCell;
        private CheckBox chkMustHaveGps;
        private TableLayoutPanel tableLayoutPanel1;
        private TextBox txtOut;
        private Panel panel1;
        private Label lblUnitCell;
        private Label lblUnitGps;
        private TextBox txtUnitCell;
        private TextBox txtUnitGps;
        private Label lblMain;
        private Label lblUnitSerial;
        private TextBox txtUnitSerial;
        private Button btnStart;
        private GroupBox grpOptions;
        private CheckBox chkProgramFirmware;
        private Label lblReplicaDatabaseAddress;
        private CheckBox chkTestingLisa;

    }
}