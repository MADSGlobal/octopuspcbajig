///
/// Simple Production Test Tool, based on Agilent's demo code
///  To find main production test loop, scroll to the end
///  
/// Straight-forward procedure-based linear code, based on VISA command structure, neither optimised nor generic.
/// 
/// (c) 2015 MADSGlobal NZ
/// 

using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Drawing;
using System.IO.Ports;
using System.IO;
using System.Media;
using System.Text.RegularExpressions;
using System.Threading;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using System.Xml;
using System.Web.UI;
using System.Management;
using CouchDbAPI;
using Ivi.Visa.Interop;
using ProdTest.Properties;
using CoreScanner;
using Utilities;


namespace ProdTest
{
    public partial class ProdTest : Form
    {
        private DaqMeter DAQ;
        private DaqPower PSU;
        private bool bTestIsRunning;     

        public ProdTest()
        {
            InitializeComponent();
        }



        /// <summary>
        ///     The main entry point for the application.
        /// </summary>
        [STAThread]
        public static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new ProdTest());
        }



        #region logging
        /// Log to screen (and file)
        /// LogLevel : (to be implemented)
        /// 0 - don't log
        /// 1 - program log only
        /// 2 - as above plus screen
        /// 3 - as above plus database
        private void LogStr(string s, int level)
        {
            if (level > 0)
            {
                new LogWrite(s);
                //! Create an event "LogChanged" for class
                txtOut.Update();
                if (level > 1)
                {
                    txtOut.AppendText(s);

                    if (level > 2)
                    {
                    }
                }
            }
            Application.DoEvents();
        }


        private void LogCol(int c)
            /// change screen color: red = -1, white, green };
        {
            if (c == -1) txtOut.BackColor = Color.Orange;
            if (c == 0) txtOut.BackColor = SystemColors.Window;
            if (c == 1) txtOut.BackColor = Color.Lime;
            txtOut.Update();
        }
        #endregion

        #region Load Save defaults
        public void LoadSettings()
        {
            if (Settings.Default.UpgradeRequired)
            {
                try
                {
                    Settings.Default.Upgrade();
                }
                catch { }
                Settings.Default.UpgradeRequired = false;
                Settings.Default.Save();
            }

            Settings.Default.Reload();

            txtSetupProdname.Text = Settings.Default.ProductName;
            txtCouchDatabaseAddress.Text = Settings.Default.CouchDatabaseAddress;
            txtAddress1.Text = Settings.Default.AddressDAQ;
            txtAddress2.Text = Settings.Default.AddressPSU;
            txtDutComPort.Text = Settings.Default.DutComPort;
            txtCouchDatabaseAddress.Text = Settings.Default.CouchDatabaseAddress;

            chkMustHaveGps.Checked = Settings.Default.MustHaveGps;
            chkTestingLisa.Checked = Settings.Default.TestingLisa;
            chkMustHaveCell.Checked = Settings.Default.MustHaveCell;
            txtControllerProgrammer.Text = Settings.Default.ControllerProgrammer;
            txtControllerApplication.Text = Settings.Default.ControllerApplication;
        }

        public void SaveSettings()
        {
            Settings.Default.ProductName = txtSetupProdname.Text;
            Settings.Default.AddressDAQ = txtAddress1.Text;
            Settings.Default.AddressPSU = txtAddress2.Text;
            Settings.Default.DutComPort = txtDutComPort.Text;
            Settings.Default.CouchDatabaseAddress = txtCouchDatabaseAddress.Text;

            Settings.Default.MustHaveGps = chkMustHaveGps.Checked;
            Settings.Default.TestingLisa = chkTestingLisa.Checked;
            Settings.Default.MustHaveCell = chkMustHaveCell.Checked;
            Settings.Default.ControllerProgrammer = txtControllerProgrammer.Text;
            Settings.Default.ControllerApplication = txtControllerApplication.Text;

            Settings.Default.Save();
        }
        #endregion

        #region GUI
        public void ProdTest_Load(object sender, EventArgs e)
        {
            this.Text = "Octopus Production Test - V" + Application.ProductVersion;
            string dts = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss");
            LogStr("=== Application started === " + dts + " ===\r\n ", 1);

            OpenBarcodeScanner();

            LoadSettings();

            DAQ = new DaqMeter(Settings.Default.NameDAQ, Settings.Default.AddressDAQ, 10);
            PSU = new DaqPower(Settings.Default.NamePSU, Settings.Default.AddressPSU, 100);

            // Assign output handlers
            DAQ.LogHandler = LogStr;
            DAQ.DatabaseHandler = (daq) => { couch.Add(daq.channelName, daq.lastResult); }; 

            // Same handlers for PSU
            PSU.LogHandler = DAQ.LogHandler;
            PSU.DatabaseHandler = DAQ.DatabaseHandler;

            ActiveControl = txtUnitSerial;
        }

        private void ProdTest_FormClosed(object sender, FormClosedEventArgs e)
        {
            string dts = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss");
            LogStr("--- Application closed --- " + dts + " ---\r\n\r\n", 1);
            SaveSettings();
        }

        private void chkTestingLisa_CheckedChanged(object sender, EventArgs e)
        {
            if (chkTestingLisa.Checked)
            {
                chkMustHaveCell.Enabled = true;
                lblUnitCell.Enabled = true;
                txtUnitCell.Enabled = true;
            }
            else
            { 
                chkMustHaveCell.Enabled = false;
                lblUnitCell.Enabled = false;
                txtUnitCell.Enabled = false;
            }
        }



        //! Hiding/disabeling would be better solution! (all 3 below)
        //!   'Save' button not used yet?!?
        private void tabControl1_Deselecting(object sender, TabControlCancelEventArgs e)
        {
            // Save settings when focus is taken away from the config tab
            if (tabControl1.SelectedTab == tabMainConfig)
            {
                SaveSettings();
            }
        }


        private void btnSaveSettings_Click(object sender, EventArgs e)
        {
            SaveSettings();
            // remove, then add in correct order
            tabControl1.Controls.Remove(tabMainConfig);
            tabControl1.Controls.Add(tabMainRun);
            tabControl1.Controls.Add(tabMainConfig);
            tabControl1.SelectedTab = tabMainRun;
            txtUnitSerial.Focus();
        }


        private void tabControl1_Selecting(object sender, TabControlCancelEventArgs e)
        {
            if (tabControl1.SelectedTab == tabMainConfig)
            {
                tabControl1.Controls.Remove(tabMainRun);
            }
        }


        private void txtUnitSerial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                ActiveControl = btnStart;
            }
        }


        private void txtLaserSerial_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyValue == 13)
            {
                ActiveControl = btnStart;
            }
        }


        #endregion

        #region LEGACY - DUT COM port for Spike
        // use WriteDutComHalveNmea for Spike, WriteDutComNmea for Popeye
        /*
        private void WriteDutComHalveNmea(string s)
        {
            string sw = Nmea.AddHalveHeaderChecksum(s);
            Debug.WriteLine("sending" + sw);
            DUTSerialPort.Write(sw + "\r"); //CR only
        }

        private bool WriteDutCommandInit()
        {
            DUTSerialPort.DiscardOutBuffer();
            DUTSerialPort.DiscardInBuffer();
            Thread.Sleep(50);
            Debug.WriteLine("sending: $");
            DUTSerialPort.Write("$");

            try
            {
                DUTSerialPort.ReadTo("?\r\n");
            }
            catch (TimeoutException)
            {
                Debug.WriteLine("did not receive ? within time limit");
                return false;
            }

            Debug.WriteLine("received: ?");
            return true;
        }

        private bool WriteDutHalveCommand(string cmd)
        {
            if (!(WriteDutCommandInit())) return false;
            WriteDutComHalveNmea(cmd);

            return true;
        }

        private string WriteAndReadHalveCommand(string cmd)
        {
            LogStr("sending command \"" + cmd + "\" to DUT ... ", 2);
            if (!(WriteDutHalveCommand(cmd)))
            {
                LogStr("FAILED: Can't send command\r\n", 2);
                return string.Empty;
            }
            LogStr("OK\r\n", 2);
            return ReadDutComNmea();
        } */
        #endregion

        #region DUT COM port
        private void CloseUart(SerialPort port)
        {
            try
            {
                if (port.IsOpen) port.Close();
            }
            catch { }
        }

        private void OpenUart(SerialPort port, int baudrate)
        {
            CloseUart(port);
            port.BaudRate = baudrate;
            port.StopBits = StopBits.One;
            port.ReadTimeout = 2000;

            try
            {
                port.Open();
            }
            catch (Exception ex)
            {
                throw (ex);
            }
            port.DiscardInBuffer();
            port.DiscardOutBuffer();
        }


        private bool OpenDutCom(int baudrate)
        {
            CloseUart(DUTSerialPort);
            DUTSerialPort.PortName = Settings.Default.DutComPort;

            try
            {
                OpenUart(DUTSerialPort, baudrate);
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Open failed on " + DUTSerialPort.PortName + " " + ex.Source + "  " + ex.Message,
                    "ProdTest", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }


        private bool OpenLisaCom(int baudrate, string portname)
        {
            CloseUart(LisaSerialPort);
            LisaSerialPort.PortName = portname;

            try
            {
                OpenUart(LisaSerialPort, 115200);
            }
            catch (Exception)
            {
                LogStr("Can't open UART port to LISA module ... FAILED!\r\n", 2);
                return false;
            }
            LogStr("UART port to LISA module is open ... OK\r\n", 2);
            return true;
        }

        
        public string WaitForComPortByName(string name, int delay)
        {
            for (int i = delay; i > 0; i-=1000)
            {
                string s = Pnp.FindComPortByName(name);
                if (s != string.Empty) return s;
                new Delay(1000);
            }
            return string.Empty;
        }


        private string UartReadLine(SerialPort port)
        {
            try
            {
                string rs = port.ReadLine();
                LogStr("rs: "+rs+"\n",2);
                rs = rs.TrimEnd('\r', '\n', ' ');
                return rs;
            }
            catch(Exception e) {
                LogStr("Exception: " + e.Message + "\n", 2);
                return string.Empty; 
            }
        }


        private string ReadDutComNmea()
        {
            string rs = UartReadLine(DUTSerialPort);
            if (rs != string.Empty) rs = Nmea.StripHeaderChecksum(rs);
            return rs;
        }


        private bool ReadCompareDutComNmea(string s)
        {
            string rs = ReadDutComNmea();
            Debug.WriteLine("receieved: " + rs);
            // filter out BLE status and battery messages!
            while ((rs == "DO") || (rs.StartsWith("$B")))
            {
                rs = ReadDutComNmea();
            }
            if (rs == s)
            {
                return true;
            }
            LogStr("result = " + rs + ",", 2);
            return false;
        }

        private bool WriteDutCommand(string cmd)
        {
            //!! move logstr here
            LogStr("sending command \"" + cmd + "\" to DUT ... ", 1);
            if ((DUTSerialPort == null) || (!DUTSerialPort.IsOpen))
            {
                LogStr("FAILED: DUT COM port is not open!\r\n", 2);
                return false;
            }

            DUTSerialPort.DiscardOutBuffer();
            DUTSerialPort.DiscardInBuffer();
            Thread.Sleep(50);

            string sw = Nmea.AddHeaderChecksum(cmd);
            Debug.WriteLine("sending " + sw);
            DUTSerialPort.Write(sw + "\r");  //CR only!
            LogStr("OK\r\n", 1);
            return true;
        }

        private string WriteAndReadCommand(string cmd)
        {
            if (!(WriteDutCommand(cmd)))
            {
                return string.Empty;
            }
            return ReadDutComNmea();
        } 

        private bool WriteAndCheckCommand(string cmd, string expected)
        {
            if (!(WriteDutCommand(cmd)))
            {
                // Retry!
                if (!(WriteDutCommand(cmd)))
                {
                    return false;
                }
            }

            if (!(ReadCompareDutComNmea(expected)))
            {
                LogStr("Command " + cmd + " did not get expected response (" + expected + ") ... \tFAILED\r\n", 2);
                return false;
            }
            LogStr("Command " + cmd + " did get expected response, ...\tOK\r\n", 2);
            return true;
        }


        /// <summary>
        ///     Analyses the DUT stream and returns the result of the 'code' and 'index'. Must appear within 10 lines
        /// </summary>
        /// <param name="cmd"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        private string ReadDutStream(string code, int index)
        {
            for (int i = 0; i < 10; i++)
            {
                //!! Use line below if $CAL strings are using proper NMEA format 
                //string s = ReadDutComNmea();
                // contingency - begin
                string s;
                try
                {
                    s = DUTSerialPort.ReadLine();
                }
                catch
                {
                    continue;
                }
                if (s.Length < 1) continue;

                int cmdStart = s.IndexOf("$") + 1;
                s = s.Substring(cmdStart, s.Length - cmdStart);
                // contingency - end

                if (s.Length < code.Length) continue;
                if (s.Substring(0, code.Length) == code)
                {
                    string[] segments = s.Split(',', '*');
                    return segments[index];
                }
            }
            return string.Empty;
        }


        private bool ReadCheckDutStreamFloat(string code, int index, float vmin, float vmax, string name)
        {
            float f;
            string smin = vmin.ToString("0.00");
            string smax = vmax.ToString("0.00");

            if (name != string.Empty) LogStr("Reading " + name + " - (" + smin + " min, " + smax + " max) ... ", 3);
            string s = ReadDutStream(code, index);
            try
            {
                f = Convert.ToSingle(s);
            }
            catch
            {
                return false;
            }
            s = f.ToString("0.00");
            couch.Add(name, s);

            if ((f > vmax) || (f < vmin))
            {
                if (name != string.Empty) LogStr("actual = " + s + " ..... \tFAILED\r\n", 3);
                return false;
            }
            if (name != string.Empty) LogStr("actual = " + s + " ..... \tOK\r\n", 3);
            return true;
        }


        /// <summary>
        ///     Writes a command and returns the stripped result or empty on error
        /// </summary>
        /// <param name="readCmd"></param>
        /// <returns>number as string or empty string if error (=is already set)</returns>
        private string ReadDutString(string readCmd)
        {
            // assuming command repetion plus "," is always used -> additional length = 1
            // Except errors which always start with an "ERR"
            string s = WriteAndReadCommand(readCmd);
            if (s.StartsWith("ERR")) return string.Empty;
            if (s.Length < readCmd.Length + 1) return string.Empty;

            s = s.Substring(readCmd.Length + 1, s.Length - (readCmd.Length + 1));
            return s;
        }

        /// <summary>
        /// Sends a command, reads the returning string and converts it into a float
        /// </summary>
        /// <param name="cmd">Command to send to DUT</param>
        /// <param name="res">Result</param>
        /// <returns>Success</returns>
        private bool ReadDutNumber(string cmd, out float res)
        {
            res = 0;

            string s = ReadDutString(cmd);
            if (s == string.Empty) return false;

                //strip non-numerics
            s = Regex.Replace(s, "[^0-9.]", "");

            try
            {
                res = Convert.ToSingle(s);              
            }
            catch { return false; }

            return true;
        }

        /// <summary>
        /// Sends a command, reads the returning string, converts it into a float and checks it against boundaries
        /// </summary>
        /// <param name="cmd">Command to send to DUT</param>
        /// <param name="rMin">Lower boundary</param>
        /// <param name="rMax">Upper boundary</param>
        /// <param name="name">Name for logging</param>
        /// <returns></returns>
        private bool ReadCheckNumber(string cmd, float rMin, float rMax, string name)
        {
            float f;
            string smin = rMin.ToString("0.00");
            string smax = rMax.ToString("0.00");

            if (name != string.Empty) LogStr("Reading " + name + " - (" + smin + " min, " + smax + " max) ... ", 3);

            if (!ReadDutNumber(cmd, out f))
            {
                if (name != string.Empty) LogStr(" ..... \tFAILED\r\n", 3);
                return false;
            }

            string s = f.ToString("0.00");
            couch.Add(name, s);

            if ((f < rMin) || (f > rMax))
            {
                if (name != string.Empty) LogStr("actual = " + s + " ..... \tFAILED\r\n", 3);
                return false;
            }
            if (name != string.Empty) LogStr("actual = " + s + " ..... \tOK\r\n", 3);
  
            return true;
        }


        private bool WriteCheckDutString(string writeCmd, string readCmd, string number, bool mustMatch, string name)
        {
            string s = ReadDutString(readCmd);

            if (s == string.Empty)
            {
                // Most likely the field is empty, so attempt to write it
                LogStr("Starting to write number to DUT: " + writeCmd + " to " + number + " ...\r\n", 2);
                if (!(WriteAndCheckCommand(writeCmd + ',' + number, writeCmd + ",OK")))
                {
                    LogStr("Finishing writing number to DUT: FAILED\r\n", 2);
                    return false;
                }
                LogStr("Finishing writing number to DUT: OK\r\n", 2);
                couch.Add(name, "programmed");
                return true;
            }
            // Could read a field, compare if needed (or just show)
            if (mustMatch)
            {
                if (s == number)
                {
                    LogStr("Number " + name + " verified to value = " + s + " ... OK\r\n", 2);
                    couch.Add(name, "verified");
                    return true;
                }
                LogStr("Number " + name + " read as " + s + " but must be " + number + " ... FAILED\r\n", 2);
                return false;
            }
            LogStr("Number " + name + " already programmed, value = " + s + " ... OK\r\n", 2);
            couch.Add(name, "pre-programmed");
            return true;
        }

        private bool ReadStoreDutString(string readCmd, string name)
        {
            LogStr("Reading " + name + " ... ", 2);
            string s = ReadDutString(readCmd);

            // Use this line if number MUST be set
            //if (s == string.Empty) return false;
            if (s == string.Empty) s = "NOT SET";

            LogStr("done, value = " + s + " ... OK\r\n", 2);
            couch.Add(name, s);
            return true;
        }

        public bool ParseCheckIntLimits(string input, int min, int max)
        {
            try
            {
                string[] s = input.Split(new char[] { '=' });
                string r = s[2].Trim();
                int i = Convert.ToInt32(r);
                if ((i < min) || (i > max)) return false;
            }
            catch { return false; }
            return true;
        }

        public bool ParseCheckDutInt(string cmd, string name, string[] startsWith, int[] max, int[] min)
        {
            LogStr("Parsing result of " + name + " ... ", 2);
            string s = ReadDutString(cmd);

            string[] items = s.Split(new char[] { ',' });
            for (int i = 0; i < items.Length; i++)
            {
                if (!items[i].StartsWith(startsWith[i])) return false;
                if (!ParseCheckIntLimits(items[i], min[i], max[i])) return false;

            }
            LogStr("done, value = " + s + " ... OK\r\n", 2);
            couch.Add(name, s);            
            return true;
        }

        public bool UartCheckLineContains(SerialPort port, string name, string compare)
        {
            string s = string.Empty;
            try
            {
                if (!(s = UartReadLine(port)).Contains(compare))
                {
                    if (!(s = UartReadLine(port)).Contains(compare))
                    {
                        LogStr(name + " received unexpected data: [" + s + "] ... FAILED!\r\n", 2);
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                LogStr(name + " can't be read: " + ex.Message + " ... FAILED!\r\n", 2);
            }
            LogStr(name +  " confirmed ... OK\r\n", 2);
            if (!name.StartsWith("_"))
            {
                couch.Add(name, "confirmed");
                couch.Add(name + " stream", s);
            }
            Debug.WriteLine("received: " + s);
            return true;
        }

        public bool UartCheckModemLineContains(SerialPort port, string command, string name, string compare)
        {
            bool r = false;
        
            try
            {
                port.DiscardInBuffer();
                port.Write(command);
                r = UartCheckLineContains(port, name, compare);
                //read extra lines, usually "\r" followed by "OK" or "ERROR"
                UartReadLine(port);
                UartReadLine(port);
            }
            catch {}
            return r;
        }

        public bool UartGetModemMfgInfo(SerialPort port, string command, string name)
        {
            bool r = false;
            string s = string.Empty;

            try
            {
                port.DiscardInBuffer();
                port.Write(command);
                UartReadLine(port); //Read first line response. Usually gets the command as response
                s = UartReadLine(port); //actual data
                couch.Add(name, s);
                Debug.WriteLine("received: " + s);
                LogStr(name + " value = " + s + "\r\n", 2);
                r = true;
            }
            catch (Exception ex)
            {
                LogStr(name + " can't be read: " + ex.Message + " ... FAILED!\r\n", 2);
                r = false;
            }
            return r;
        }
        #endregion


        #region DAQ and PSU devices
        private void btnInitMeter1_Click(object sender, EventArgs e)
        {
            DAQ.Close();
            try
            {
                txtIDStr1.Text = "Scanning...";
                DAQ.Open();
                txtIDStr1.Text = DAQ.GetID();
            }
            catch (Exception ex) { txtIDStr1.Text = ex.Message; }
            finally { DAQ.Close(); }
        }


        private void btnInitMeter2_Click(object sender, EventArgs e)
        {
            PSU.Close();
            try
            {
                txtIDStr2.Text = "Scanning...";
                PSU.Open();
                txtIDStr2.Text = PSU.GetID();
            }
            catch (Exception ex) { txtIDStr2.Text = ex.Message; }
            finally { PSU.Close(); }
        }
        #endregion

        #region programmer

        private string GetFileDialogString(string filter, string initialDirectory)
        {
            var fileDialog = new OpenFileDialog();

            // Set filter options and filter index.
            fileDialog.Filter = filter;
            fileDialog.FilterIndex = 1;
            fileDialog.InitialDirectory = initialDirectory;

            // Call the ShowDialog method to show the dialog box.
            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                return fileDialog.FileName;
            }
            return string.Empty;
        }

        private string GetFileDialogString(string filter)
        {
            return GetFileDialogString(filter, Environment.CurrentDirectory);
        }


        private bool CallProgJlink(char cmd)
        {
             // nrfjprog -r 
             // nrfjprog -r -e --clockspeed 2000 --program "bootloader.hex" "application.hex" --verify
            
            string output;
            Process p;
            string cmdStr = string.Empty;

            if (cmd == 'R')
            {
                cmdStr = "resetting";
            }
            if (cmd == 'E')
            {
                cmdStr = "erasing";
            }
            if (cmd == 'A')
            {
                cmdStr = "programming application, adding protection";
            }
            LogStr("Calling Jlink flasher for " + cmdStr + " ... \r\n", 2);

            try
            {
                string appPath = Environment.CurrentDirectory;

                p = new Process();
//                p.StartInfo.FileName = System.Environment.GetFolderPath(System.Environment.SpecialFolder.ProgramFiles) + @"\Nordic Semiconductor\nrf51\bin\nrfjprog.exe";
                p.StartInfo.FileName = Settings.Default.ControllerProgrammer;
                //! make firmware location configurable
                if (cmd == 'R')
                    p.StartInfo.Arguments = @" -r";
                if (cmd == 'E')
                    p.StartInfo.Arguments = @" -r -e --clockspeed 2000";
                if (cmd == 'A')
                    p.StartInfo.Arguments = @" -r --clockspeed 2000 --program """ 
                        + Settings.Default.ControllerApplication +@""" --rbp ALL --verify";               
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                output = p.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Open failed on calling Jlink flasher " + ex.Source + "  " + ex.Message, "ProdTest",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogStr("FAILED\r\n", 2);
                return false;
            }

            // This is potentially dangerous as it might wait forever!
            p.WaitForExit();
            LogStr("\r\n" + output + "\r\n ... done, result = ", 2);

            if (p.ExitCode == 0)
            {
                if (cmd == 'E')
                {
                    couch.Add("Firmware, erased", true.ToString());
                }
                if (cmd == 'S')
                {
                    couch.Add("Firmware, softdevice", true.ToString());
                }
                if (cmd == 'A')
                {
                    couch.Add("Firmware, application", true.ToString());
                }
                LogStr("Ok\r\n", 2);
                return true;
            }
            LogStr("exitcode = " + p.ExitCode + " - FAILED\r\n", 2);
            return false;
        }

        private void btnControllerProgrammer_Click(object sender, EventArgs e)
        {
            string s = GetFileDialogString("Nordic programmer|nrfjprog.exe|Exe Files (.exe)|*.exe|All Files (*.*)|*.*", Path.GetDirectoryName(Settings.Default.ControllerProgrammer));
            if (s != string.Empty)
            {
                txtControllerProgrammer.Text = s;
            }
        }

        private void btnControllerApplication_Click(object sender, EventArgs e)
        {
            string s = GetFileDialogString("Application Files (.hex)|*.hex|All Files (*.*)|*.*", Path.GetDirectoryName(Settings.Default.ControllerApplication));
            if (s != string.Empty)
            {
                txtControllerApplication.Text = s;
            }

        }


        public string BleDeviceID = string.Empty;
        private bool CallProgBle(char cmd)
        {
            string output;
            Process p;
            string cmdStr = string.Empty;

            if (cmd == 'G')
            {
                cmdStr = "reading parameters";
            }
            if (cmd == 'P')
            {
                cmdStr = "programming";
            }
            LogStr("Calling BLE flasher for " + cmdStr + " ... \r\n", 2);

            try
            {
                string appPath = Environment.CurrentDirectory;

                p = new Process();
                p.StartInfo.FileName = appPath + @"\BLEUpdate\bleupdate-cli.exe";
                //! make firmware location configurable
                if (cmd == 'P') p.StartInfo.Arguments = @"update ..\firmware\SpikeBluetooth.hex";
                if (cmd == 'G') p.StartInfo.Arguments = @"get";
                p.StartInfo.WorkingDirectory = appPath + @"\BLEUpdate";

                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                output = p.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Open failed on calling AtMega flasher " + ex.Source + "  " + ex.Message, "ProdTest",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogStr("FAILED\r\n", 2);
                return false;
            }

            // This is potentially dangerous as it might wait forever!
            p.WaitForExit();

            if (p.ExitCode == 0)
            {
                if (cmd == 'G')
                {
                    LogStr("\r\n" + output + "\r\n ... done, result = ", 2);

                    // simple decoding of lines 2-4 
                    string[] lines = output.Split('\r', '\n');

                    // every 2nd line is empty
                    string[] pairs = lines[2].Split(':');
                    pairs[1] = pairs[1].TrimStart(' ');
                    couch.Add("BLE address", pairs[1] + ":" + pairs[2] + ":" +
                                             pairs[3] + ":" + pairs[4] + ":" +
                                             pairs[5] + ":" + pairs[6]);
                    BleDeviceID = pairs[1] + pairs[2] + pairs[3] + pairs[4] + pairs[5] + pairs[6];

                    pairs = lines[4].Split(':');
                    pairs[1] = pairs[1].TrimStart(' ');
                    couch.Add("BLE serial", pairs[1]);

                    pairs = lines[6].Split(':');
                    pairs[1] = pairs[1].TrimStart(' ');
                    couch.Add("BLE license", pairs[1]);
                }
                if (cmd == 'P')
                {
                    LogStr("\r\n ... done, result = ", 2);
                    couch.Add("Firmware, BLE", true.ToString());
                }
                LogStr("Ok\r\n", 2);
                return true;
            }
            LogStr("exitcode = " + p.ExitCode + " - FAILED\r\n", 2);
            return false;
        }

        private bool CallReadBle(string ModuleID)
        {
            string output;
            Process p;
            string cmdStr = string.Empty;
            string sSerial = string.Empty;

            LogStr("Calling BLEinfo ... \r\n", 2);

            if (ModuleID.Length != 12) return false;
            ModuleID = ModuleID.ToUpper();

            // testing for string format = "V1.46A000012,1123";
            string s = txtUnitSerial.Text;
            try
            {
                // Chop off beginning, then end
                s = Regex.Replace(s, "^V[0-9]+[.][0-9]+[A-Z]", "");
                s = Regex.Replace(s, ",[0-9]+", "");
                s = s.PadLeft(7, '0');
                sSerial = s;

                int i = Convert.ToInt32(s);
            }
            catch
            {
                LogStr("Can't decode serial number - FAILED!\r\n", 2);
                return false;
            }

            try
            {
                string appPath = Environment.CurrentDirectory;

                p = new Process();
                p.StartInfo.FileName = appPath + @"\BLEinfo\bleinfo.exe";
                //! make firmware location configurable

                // JG - Disabling writing serial number to bluetooth until it's been tested through factory.
                p.StartInfo.Arguments = ModuleID + " \"SPIKE " + sSerial + "\"";
                //p.StartInfo.Arguments = ModuleID + " \"Spike by ike\"";

                p.StartInfo.WorkingDirectory = appPath + @"\BLEinfo";

                p.StartInfo.UseShellExecute = false;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.CreateNoWindow = true;
                p.Start();

                output = p.StandardOutput.ReadToEnd();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Open failed on calling BLEinfo " + ex.Source + "  " + ex.Message, "ProdTest",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                LogStr("FAILED\r\n", 2);
                return false;
            }

            // This is potentially dangerous as it might wait forever!
            p.WaitForExit();

            if (p.ExitCode == 0)
            {
                LogStr("\r\n" + output + "\r\n ... done, result = ", 2);

                // simple decoding of lines 2-4 
                string[] lines = output.Split('\r', '\n');

                // some empty lines
                string[] pairs = lines[2].Split(':');

                pairs = lines[6].Split(':');
                pairs[1] = pairs[1].TrimStart(' ');
                couch.Add("BLE model", pairs[1]);

                pairs = lines[8].Split(':');
                pairs[1] = pairs[1].TrimStart(' ');
                couch.Add("BLE firmware version", pairs[1]);
                LogStr("Ok\r\n", 2);
                return true;
            }
            LogStr("exitcode = " + p.ExitCode + " - FAILED\r\n", 2);
            return false;
        }
        #endregion
        
        #region Barcode scanner
        HardwareWrapper.BarcodeScanner Scanner;

        private void OpenBarcodeScanner()
        {
            Scanner = new HardwareWrapper.BarcodeScanner();
            var res = Scanner.Init();
            if (!res.isOK)
            {
                MessageBox.Show("Can't open barcode scanner:\r\n\r\n" + res.errorString, "ProdTest",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Scanner.BarcodeScannerEvent += Scanner_BarcodeScannerEvent;      
        }



        public string HardwareRevisionMain = string.Empty;
        public string SerialNumberMain = string.Empty;
        public string BuildStrMain = string.Empty;
        public string HardwareRevisionLisa = string.Empty;
        public string SerialNumberLisa = string.Empty;
        public string BuildStrLisa = string.Empty;
        public string HardwareRevisionGps = string.Empty;
        public string SerialNumberGps = string.Empty;
        public string BuildStrGps = string.Empty;
       
        //!! Check numbers when leaving fields (manual entry)
        void Scanner_BarcodeScannerEvent(object sender, GenericEventArgs.ValueEventArgs<string[]> e)
        {
            if (!bTestIsRunning)
            {
                statusLabel.Text = "Scanning barcode ...";

                string hw, serial, build;
                HardwareWrapper.BarcodeScanner.DecodeBarcodeForIke(e.Value[0], out hw, out serial, out build);

                if (serial.StartsWith("0"))
                {
                    HardwareRevisionMain = hw;
                    SerialNumberMain = serial;
                    BuildStrMain = build;
                    statusLabel.Text = "Main barcode label identified";
                }
                else if (serial.StartsWith("1"))
                {
                    HardwareRevisionGps = hw;
                    SerialNumberGps = serial;
                    BuildStrGps = build;
                    statusLabel.Text = "Limpet barcode label identified";
                }
                else if (serial.StartsWith("2"))
                {
                    HardwareRevisionLisa = hw;
                    SerialNumberLisa = serial;
                    BuildStrLisa = build;
                    statusLabel.Text = "Lisa barcode label identified";
                }
                else if (e.Value[0].StartsWith("SN"))
                {
                    statusLabel.Text = "Septentrio(r) barcode label identified";
                    HardwareRevisionGps = "empty";
                    SerialNumberGps = e.Value[0];
                    BuildStrGps = "empty";
                }
                else if (e.Value[0].StartsWith("GR"))
                {
                    statusLabel.Text = "Septentrio(r) barcode label identified, please scan the other barcode (starting with SN)";
                    SerialNumberGps = string.Empty;
                    HardwareRevisionGps = string.Empty;
                    BuildStrGps = string.Empty;
                }
                    // This is just an assumption, all NEO module so far did start with "36"
                else if (e.Value[0].StartsWith("36"))
                {
                    statusLabel.Text = "u-Blox(r) NEO barcode label identified, please scan PCB barcode, not module barcode";
                    SerialNumberGps = string.Empty;
                    HardwareRevisionGps = string.Empty;
                    BuildStrGps = string.Empty;
                }
                else if (e.Value[0].Contains("LISA"))
                {
                    statusLabel.Text = "u-Blox(r) LISA barcode label identified, please scan PCB barcode, not module barcode";
                    SerialNumberLisa = string.Empty;
                    HardwareRevisionLisa = string.Empty;
                    BuildStrLisa = string.Empty;
                }
                else statusLabel.Text = "Unknown barcode label";

                ShowSerialNumbers();
                if (ValidateSerialNumbers())
                {
                    Invoke((MethodInvoker)delegate {
                        btnStart.Enabled = true;
                        btnStart.Focus(); 
                    });
                }
                else Invoke((MethodInvoker)delegate { btnStart.Enabled = false; }); 
            }
        }

        void ShowSerialNumbers()
        {
            Invoke((MethodInvoker)delegate { 
                txtUnitSerial.Text = HardwareRevisionMain + SerialNumberMain;
                txtUnitGps.Text = HardwareRevisionGps + SerialNumberGps;
                txtUnitCell.Text = HardwareRevisionLisa + SerialNumberLisa;
            });
        }


        bool ValidateSerialNumbers()
        {
            if (SerialNumberMain == string.Empty) return false;
            if (chkMustHaveGps.Checked)  { if (SerialNumberGps == string.Empty) return false; }
            if (chkTestingLisa.Checked)
            {
                if (chkMustHaveCell.Checked) { if (SerialNumberLisa == string.Empty) return false; }
            }
            return true;
        }
        #endregion

        #region Background worker
        public bool waitForVoltageCancelled;
        public OperatorActionRequest actionRequest;

        private void backgroundGetPowerbutton_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
        }

        private void backgroundGetPowerbutton_DoWork(object sender, DoWorkEventArgs e)
        {
            actionRequest = new OperatorActionRequest();
            actionRequest.TopMost = true;
            actionRequest.Show();
            actionRequest.BringToFront();
            actionRequest.Focus();
            actionRequest.BackColor = Color.DarkCyan;
            actionRequest.Activate();

            //!! Make this an event?
            while ((actionRequest.DialogResult != DialogResult.Cancel) &&
                   (!(backgroundGetPowerbutton.CancellationPending)))
            {
                if (actionRequest.BackColor == Color.DarkCyan)
                    actionRequest.BackColor = Color.Cyan;
                else
                    actionRequest.BackColor = Color.DarkCyan;
                Application.DoEvents();
                new Delay(500);
            }
            actionRequest.BackColor = Color.Lime;
            waitForVoltageCancelled = true;
            actionRequest.Close();
        }


        /// <summary>
        ///     Checks a voltage and opens a requester for the operator to cancel the task.
        /// </summary>
        /// <param name="chan"></param>
        /// <param name="vmin"></param>
        /// <param name="vmax"></param>
        /// <param name="question"></param>
        /// <returns></returns>
        private bool WaitForVoltage(int chan, float vmin, float vmax, string question)
        {
            LogStr("Asking for user interaction: " + question + " ... ", 2);

            waitForVoltageCancelled = false;
            SystemSounds.Asterisk.Play();
            backgroundGetPowerbutton.RunWorkerAsync();
            while (!(waitForVoltageCancelled))
            {
                if (DAQ.GetVoltage(chan, vmin, vmax, "", 200))
                {
                    // close requester
                    backgroundGetPowerbutton.CancelAsync();
                    LogStr("OK\r\n", 2);
                    return true;
                }
                
                Application.DoEvents();
                new Delay(500);
            }
            LogStr("FAILED\r\n", 2);
            return false;
        }
        #endregion

        #region Main loop wrapper and support
        private void ShowException(Exception ex)
        {
            LogCol(-1);
            LogStr(" ----- SYSTEM ERROR -----\r\n", 2);
            if (ex != null)
            {
                var dialog = MessageBox.Show(ex.Message + "\r\n\r\nClick 'Retry' for more information", "Error", 
                                            MessageBoxButtons.RetryCancel, MessageBoxIcon.Error);
                if (dialog == System.Windows.Forms.DialogResult.Retry)
                {
                    string exMessage;
                    if (ex.InnerException != null)
                    {
                        exMessage = "Exception: " + ex.Message + "\r\n\r\n"
                                    + "Inner exception: " + ex.InnerException.ToString() + "\r\n\r\n"
                                    + "Stacktrace: " + ex.StackTrace.ToString();
                    }
                    else
                    {
                        exMessage = "Exception: " + ex.Message + "\r\n\r\n"
                                    + "No inner exception!\r\n\r\n"
                                    + "Stacktrace: " + ex.StackTrace.ToString();
                    }
                    MessageBox.Show(exMessage, "System Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }


        private void ClearSerialNumbers()
        {
            btnStart.Enabled = false;
            txtUnitSerial.Enabled = true;
            txtUnitGps.Enabled = true;
            txtUnitCell.Enabled = true;
            HardwareRevisionMain = string.Empty;
            SerialNumberMain = string.Empty;
            BuildStrMain = string.Empty;
            HardwareRevisionGps = string.Empty;
            SerialNumberGps = string.Empty;
            BuildStrGps = string.Empty;
            HardwareRevisionLisa = string.Empty;
            SerialNumberLisa = string.Empty;
            BuildStrLisa = string.Empty;
            txtUnitSerial.Text = string.Empty;
            txtUnitGps.Text = string.Empty;
            txtUnitCell.Text = string.Empty;
        }


        private void btnStart_Click(object sender, EventArgs e)
        {
            if (!bTestIsRunning)
            {
                bTestIsRunning = true;
                txtOut.Text = "Initialising .....\r\n";
                txtOut.Update();
                lblMain.BackColor = Color.Transparent;
                txtUnitSerial.Enabled = false;
                txtUnitGps.Enabled = false;
                txtUnitCell.Enabled = false;

                if (MainLoopOpen())
                {
                    try
                    {
                        if (MainLoopInit())
                        {
                            bool passedMainLoop = false;
                            int mainLoopResult = int.MaxValue;
                            // Start of test commands
                            {
                                mainLoopResult = MainLoop();
                                if (mainLoopResult == MAIN_LOOP_PASSED)
                                {
                                    passedMainLoop = true;
                                }
                            }
                            if (passedMainLoop)
                            {
                                LogCol(1);
                                LogStr(" ----- PASS -----\r\n", 2);
                            }
                            else
                            {
                                LogCol(-1);
                                LogStr(" ----- Did fail at line " + mainLoopResult.ToString() + " ----- FAILED -----\r\n", 2);
                            }

                            // End of test commands
                            couch.Add("TestFinalResult", mainLoopResult.ToString());
                            MainLoopExit();
                        }
                        else LogCol(-1);
                    }
                    catch (Exception ex) {
                        ShowException(ex);
                        ClearSerialNumbers();
                        ResetPsuAndDmm();
                    }
                }
                else
                {
                    LogCol(-1);
                    LogStr(" ----- FAILED -----\r\n", 2);
                }

                MainLoopclose();
                txtUnitSerial.Focus();
                bTestIsRunning = false;
                ClearSerialNumbers();
            }
        }

        

        public CouchDBInteractions couch;
        private const int MAIN_LOOP_PASSED = 0;

        public bool MainLoopOpen()
        {
            try {
                if (txtSetupOperator.Text == string.Empty)
                {
                    var getOperator = new OperatorDialog();
                    getOperator.Owner = this;
                    getOperator.txtRequest = "Please enter Operator's name:";
                    getOperator.ShowDialog();
                    txtSetupOperator.Text = getOperator.txtReturn;
                }

                if (!DAQ.Open().isOK) return false;
                if (!PSU.Open().isOK) return false;
                if (!(OpenDutCom(115200))) return false;

                couch = new CouchDBInteractions(Settings.Default.CouchDatabaseAddress);
                if (couch == null) return false;

                // Verification that all neccessary numbers and codes are enterd or scanned not needed, 
                // re-activate if manually entered numbers are required
            }
            catch (Exception ex) { 
                ShowException(ex);
                ClearSerialNumbers();
                return false;
            }
            return true;
        }

        public void MainLoopclose()
        {
            if (couch != null) couch.Flush();
            DAQ.Close();
            PSU.Close();
            CloseUart(DUTSerialPort);
            CloseUart(LisaSerialPort);
        }


        public bool mainPcbaDetected;
        public float jigTemp;
        /// <summary>
        ///     Initialisation before Main Loop gets called
        /// </summary>
        /// <returns></returns>
        public bool MainLoopInit()
        {
            string dts = DateTime.Now.ToString("dd-MM-yyyy, HH:mm:ss");

            LogStr(
                "=== Start of Production Test, Unit# = " + HardwareRevisionMain + SerialNumberMain + ", Build = " + BuildStrMain
                + ", Laser " + HardwareRevisionLisa + ", Date = " + dts + " ===\r\n", 2);
            LogCol(0);

            if (!DetectPcb()) return false;

            couch.Add("DateTime", dts);
            couch.Add("Main PCBA Serial Number", SerialNumberMain);
            couch.Add("Main PCBA Build", BuildStrMain);
            couch.Add("Main PCBA Hardware Revision", HardwareRevisionMain);
            couch.Add("GPS: PCBA Serial Number", SerialNumberGps);
            couch.Add("GPS: PCBA Build", BuildStrGps);
            couch.Add("GPS: PCBA Hardware Revision", HardwareRevisionGps);
            if (chkTestingLisa.Checked)
            {
                couch.Add("CELL: PCBA Serial Number", SerialNumberLisa);
                couch.Add("CELL: PCBA Build", BuildStrLisa);
                couch.Add("CELL: PCBA Hardware Revision", HardwareRevisionLisa);
            }
            else couch.Add("CELL: PCBA Serial Number", "Not fitted");
            couch.Add("Operator", txtSetupOperator.Text);
            couch.Add("Product", txtSetupProdname.Text);

            /*
            float tempVolt;
            if (!DAQ.ReadVoltage(122, "_Jig temperature voltage", out tempVolt)) return false;
            jigTemp = ((int)(tempVolt * 1000));
            jigTemp /= 10;
            LogStr("Jig temperature = " + jigTemp + "degC\r\n", 2);
            couch.Add("Temperature, Jig", jigTemp.ToString());
            */
            ResetPsuAndDmm();
            return true;
        }

        void PerformPowerCycle() 
        {
            DAQ.RelaisClose(207, 0, "Drain DUT voltage via 12R");
            Thread.Sleep(2000);
            DAQ.RelaisOpen(207, 0, "Power up DUT again");
            Thread.Sleep(1000);
            DAQ.RelaisClose(209, 500, "Trigger power button");
            DAQ.RelaisOpen(209, 0, "Trigger power button");
            Thread.Sleep(3000);
            WriteAndCheckCommand("BTS,300", "BTS,OK");
        }

        /// <summary>
        ///     Exit after Main Loop was processed
        ///     for instace to shut down power, etc
        /// </summary>
        /// <returns></returns>
        public void MainLoopExit()
        {
            ResetPsuAndDmm();
            LogStr("=== End of Production Test, Unit# = " + txtUnitSerial.Text + " ===\r\n", 2);
            txtUnitSerial.Text = String.Empty;
            txtUnitGps.Text = string.Empty;
            txtUnitCell.Text = string.Empty;
            lblMain.BackColor = Color.Transparent;
        }

        public bool CheckSerialPort()
        {
            LogStr("Testing UART debug interface to DUT ... ", 2);
            string s = ReadDutString("LBR");
            if (s == string.Empty)
            {
                LogStr("Received wrong or no result - FAILED!\r\n", 2);
                return false;
            }
            LogStr("Received expected result - Ok\r\n", 2);
            couch.Add("UART port", true.ToString());
            return true;
        }


        private static int LineNumber()
        {
            const int PREVIOUS_FRAME_INDEX = 1;

            // Note: This will only work in release if the PDB file is deployed alongside the application.
            // Ref http://stackoverflow.com/questions/3137863/is-stacktrace-information-available-in-net-release-mode-build
            return new StackTrace(true).GetFrame(PREVIOUS_FRAME_INDEX).GetFileLineNumber();
        }
        #endregion

        #region Macros
        public void ResetPsuAndDmm()
        {
            try
            {
                PSU.SendCommand("*RST", 200, 1, "to reset PSU");
                DAQ.SendCommand("*RST", 50, 1, "to reset DAQ");
                Thread.Sleep(200);
            }
            catch (Exception) { }
        }

        public bool DetectPcb()
        {
            ResetPsuAndDmm();
            mainPcbaDetected = false;
            LogStr("PCB presence detection\r\n", 2);
            if (!(PSU.EnableOutput(true, 0))) return false;
            if (!(DAQ.RelaisOpen(203, 10, "_connect DAQ_COM to GND"))) return false;
            if (!(DAQ.RelaisClose(309, 50, "_connect PCB_PROBE to P1+"))) return false;
            if (!(PSU.SetVoltage(3F, 0.05F, 2.9F, 3.1F, "_PCB_PROBE supply", 0))) return false;
            if (!(DAQ.GetVoltage(123, -.1F, 0.3F, "_PCB detection voltage"))) lblMain.BackColor = Color.OrangeRed;
            else {
                mainPcbaDetected = true;
                lblMain.BackColor = Color.LimeGreen;
            }
            ResetPsuAndDmm();
            return mainPcbaDetected;
        }


        private bool SimulateBattery(float current)     // using P2
        {
            if (!(PSU.EnableOutput(false, 0))) return false;
            if (!(DAQ.RelaisClose(212, 0, "_Tie LISA_RST to BAT_P"))) return false;
            if (!(DAQ.RelaisOpen(203, 0, "_Link DAQ_COM to GND"))) return false;
            if (!(DAQ.RelaisOpen(205, 0, "_Link PSU2 return (P2-) to GND"))) return false;
            if (!(DAQ.RelaisClose(206, 200, "_Connect PSU2 to BAT_P"))) return false;
            if (!(PSU.SelectChannel(2, 0, 1))) return false;
            if (!(PSU.EnableOutput(true, 0))) return false;
            if (!(PSU.SetVoltage(3.7F, current, 3.65F, 3.75F, "_VBAT (to GND), applied (3.70V)", 0))) return false;
            return true;
        }

        private bool SimulateBareBattery(float current)     // using P2
        {
            if (!(PSU.EnableOutput(false, 0))) return false;
            if (!(DAQ.RelaisClose(212, 0, "_Tie LISA_RST to BAT_P"))) return false;
            if (!(DAQ.RelaisClose(203, 0, "_Link DAQ_COM to BAT_N"))) return false;
            if (!(DAQ.RelaisClose(205, 0, "_Link PSU2 return (P2-) to BAT_N"))) return false;
            if (!(DAQ.RelaisClose(206, 200, "_Connect PSU2 to BAT_P"))) return false;
            if (!(PSU.SelectChannel(2, 0, 1))) return false;
            if (!(PSU.EnableOutput(true, 0))) return false;
            if (!(PSU.SetVoltage(3.7F, current, 3.65F, 3.75F, "_VBAT (to BAT_N), applied (3.70V)", 0))) return false;
            return true;
        }

        private bool SimulateExternalPower(float current)       // using P1
        {
            if (!(PSU.EnableOutput(false, 0))) return false;
            if (!(DAQ.RelaisClose(212, 0, "_Tie LISA_RST to BAT_P"))) return false;
            if (!(DAQ.RelaisOpen(203, 0, "_Link DAQ_COM to GND"))) return false;
            if (!(DAQ.RelaisOpen(204, 0, "_Link PSU1 return (P1-) to GND"))) return false;
            if (!(DAQ.RelaisClose(211, 200, "_Connect PSU1 to Vin"))) return false;
            if (!(PSU.SelectChannel(1, 0, 1))) return false;
            if (!(PSU.SetVoltage(0F, current, -0.1F, 0.1F, "_Vin off (to GND)", 0))) return false;
            if (!(PSU.EnableOutput(true, 0))) return false;
            return true;
        }

        /// <summary>
        /// Switch DutCom to module
        /// </summary>
        /// <param name="channel">0=GPS1, 1=GPS2, 2=nRF, 3=IMU</param>
        /// <returns></returns>
        private bool SelectUartChannel(int channel)
        {
            switch (channel)
            {
                case 0:
                    LogStr("Selecting UART channel 0, port=GPS1\r\n", 2);
                    if (!(DAQ.RelaisOpen(310, 0, "_Relay 310 open"))) return false;
                    if (!(DAQ.RelaisOpen(311, 0, "_Relay 311 open"))) return false;
                    if (!(DAQ.RelaisOpen(312, 0, "_Relay 312 open"))) return false;
                    if (!(DAQ.RelaisOpen(313, 0, "_Relay 313 open"))) return false;
                    if (!(DAQ.RelaisOpen(314, 0, "_Relay 314 open"))) return false;
                    if (!(DAQ.RelaisOpen(315, 0, "_Relay 315 open"))) return false;
                    break;
                case 1:
                    LogStr("Selecting UART channel 1, port=GPS2\r\n", 2);
                    if (!(DAQ.RelaisOpen(310, 0, "_Relay 310 open"))) return false;
                    if (!(DAQ.RelaisClose(311, 0, "_Relay 311 closed"))) return false;
                    if (!(DAQ.RelaisOpen(312, 0, "_Relay 312 open"))) return false;
                    if (!(DAQ.RelaisOpen(313, 0, "_Relay 313 open"))) return false;
                    if (!(DAQ.RelaisClose(314, 0, "_Relay 314 closed"))) return false;
                    if (!(DAQ.RelaisOpen(315, 0, "_Relay 315 open"))) return false;
                    break;
                case 2:
                    LogStr("Selecting UART channel 2, port=nRF\r\n", 2);
                    if (!(DAQ.RelaisClose(310, 0, "_Relay 310 closed"))) return false;
                    if (!(DAQ.RelaisOpen(311, 0, "_Relay 311 open"))) return false;
                    if (!(DAQ.RelaisOpen(312, 0, "_Relay 312 open"))) return false;
                    if (!(DAQ.RelaisClose(313, 0, "_Relay 313 closed"))) return false;
                    if (!(DAQ.RelaisOpen(314, 0, "_Relay 314 open"))) return false;
                    if (!(DAQ.RelaisOpen(315, 0, "_Relay 315 open"))) return false;
                    break;
                case 3:
                    LogStr("Selecting UART channel 3, port=IMU\r\n", 2);
                    if (!(DAQ.RelaisClose(310, 0, "_Relay 310 closed"))) return false;
                    if (!(DAQ.RelaisClose(311, 0, "_Relay 311 closed"))) return false;
                    if (!(DAQ.RelaisClose(312, 0, "_Relay 312 closed"))) return false;
                    if (!(DAQ.RelaisClose(313, 0, "_Relay 313 closed"))) return false;
                    if (!(DAQ.RelaisClose(314, 0, "_Relay 314 closed"))) return false;
                    if (!(DAQ.RelaisClose(315, 0, "_Relay 315 closed"))) return false;
                    break;
                default:
                    LogStr("UART channels are supported between 0 and 3\r\n", 2);
                    return false;
            }
            return true;
        }

        private int CheckCellModem()
        {
            if (!(PSU.SetVoltage(3.7F, 2.0F, 3.65F, 3.75F, "_Increase current for VBAT (to GND) to 2A (3.70V)", 0))) return LineNumber();
            if (!(DAQ.RelaisOpen(212, 200, "Release LISA_RST from BAT_P"))) return LineNumber();
            if (!(PSU.GetCurrent(0F, 0.06F, "CELL: VBAT current, Octopus off, LISA enabled (to GND at 3.7V)", 0))) return LineNumber();
            LogStr("Waiting for modem to start (up to 15s)\r\n", 2);
            string LisaPortName = WaitForComPortByName("Modem USB2 AT", 15000);
            if (LisaPortName == string.Empty) return LineNumber();
            LogStr("Modem UART port found: " + LisaPortName + "\r\n", 2);
            new Delay(3000);
            if (!OpenLisaCom(115200, LisaPortName)) return LineNumber();
            if (!(UartCheckModemLineContains(LisaSerialPort, "AT+CGMI\r\n", "CELL: Read manufacturer", "blox"))) return LineNumber();
            if (!(UartCheckModemLineContains(LisaSerialPort, "AT+CGMM\r\n", "CELL: Read model number", "LISA"))) return LineNumber();
            if (!(UartGetModemMfgInfo(LisaSerialPort, "AT+CGMR\r\n", "CELL: Firmware Version"))) return LineNumber();
            if (!(UartGetModemMfgInfo(LisaSerialPort, "AT+CGSN\r\n", "CELL: IMEI Number"))) return LineNumber();
            CloseUart(LisaSerialPort);
            if (!(DAQ.RelaisClose(212, 0, "Tie LISA_RST to BAT_P again"))) return LineNumber();
            if (!(PSU.SetVoltage(3.7F, 0.08F, 3.65F, 3.75F, "_Decrease current for VBAT (to GND) to 80mA (3.70V)", 0))) return LineNumber();
            return 0;
        }
        #endregion

        #region Main loop
        private int MainLoop()
        {         
            // 2.3 - power supply
            LogStr(" >>> Testing 2.3: Power supply\r\n", 2);
            if (!SimulateBattery(0.3F)) return LineNumber();
            if (!(PSU.SetVoltage(3.7F, 0.3F, 3.65F, 3.75F, "VBAT (to GND), applied (3.70V)", 0))) return LineNumber();
            if (!(PSU.GetCurrent(0F, 0.005F, "VBAT current, no software, Octopus off (to GND at 3.7V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(101, 3.65F, 3.75F, "V_SYS, Octopus off"))) return LineNumber();

            if (!(DAQ.RelaisClose(307, 200, "_Enable Octopus by connecting V_SYS to LASER_PWR"))) return LineNumber();
            if (!(PSU.GetCurrent(0F, 0.025F, "VBAT current, no software, Octopus enabled (to GND at 3.7V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(102, 3.20F, 3.40F, "V_U2C from battery, no load"))) return LineNumber();

            // 2.4 - Laser
            LogStr(" >>> Testing 2.4: Laser power and UART buffer\r\n", 2);
            if (!(DAQ.GetVoltage(108, 3.20F, 3.40F, "V_LASER"))) return LineNumber();

            // 2.4.2 - Laser_Txd
            if (!(DAQ.RelaisOpen(304, 100, "Disconnect LASER_TXD_S from GND"))) return LineNumber();
            if (!(DAQ.GetVoltage(110, -.05F, .05F, "LASER_TXD, low"))) return LineNumber();
            if (!(DAQ.RelaisClose(304, 100, "Connect LASER_TXD_S to V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(110, 3.20F, 3.40F, "LASER_TXD, high"))) return LineNumber();
            if (!(DAQ.RelaisOpen(304, 0, "_Disconnect LASER_TXD_S from GND"))) return LineNumber();

            // 2.4.2 - Laser_Rxd
            if (!(DAQ.RelaisOpen(303, 100, "Disconnect LASER_RXD from GND"))) return LineNumber();
            if (!(DAQ.GetVoltage(111, -.05F, .05F, "LASER_RXD_S, low"))) return LineNumber();
            if (!(DAQ.RelaisClose(303, 100, "Connect LASER_RXD to V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(111, 3.20F, 3.40F, "LASER_RXD_S, high"))) return LineNumber();
            if (!(DAQ.RelaisOpen(303, 0, "_Disconnect LASER_RXD from GND"))) return LineNumber();

            if (!(DAQ.RelaisOpen(307, 0, "_Disable Octopus by removing V_SYS from LASER_PWR"))) return LineNumber();

            // 2.5 - IMU
            LogStr(" >>> Testing 2.5: IMU power, reset, prog and UART communication (receiving)\r\n", 2);
            if (!(DAQ.RelaisClose(308, 200, "_Enable Octopus by connecting V_SYS to IMU_PWR"))) return LineNumber();
            if (!(PSU.GetCurrent(0F, 0.08F, "VBAT current, no software, Octopus and IMU enabled (to GND at 3.7V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(109, 3.20F, 3.40F, "V_IMU"))) return LineNumber();

            // 2.5.2 - IMU_RST
            if (!(DAQ.RelaisClose(302, 100, "Connect IMU_RST to V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(104, -.05F, .05F, "/IMU_RST, low"))) return LineNumber();
            if (!(DAQ.RelaisOpen(302, 100, "Disconnect IMU_RST from V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(104, 2.5F, 3.4F, "/IMU_RST, high"))) return LineNumber();

            // 2.5.3 - IMU_PROG
            new Delay(2000);    //make sure we don't actually program the IMU!
            if (!(DAQ.RelaisClose(301, 100, "Connect IMU_PGM to V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(103, 3.2F, 3.4F, "IMU_PGM, high"))) return LineNumber();
            if (!(DAQ.RelaisOpen(301, 100, "Disconnect IMU_PGM from V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(103, -.05F, 3.4F, "IMU_PGM, low"))) return LineNumber();

            // 2.5.4 - IMU comms
            if (!SelectUartChannel(3)) return LineNumber();

            if (!WriteDutCommand("VNWRG,06,14")) return LineNumber(); //set async output message to $VNYMR;

            if (!WriteDutCommand("VNASY,0")) return LineNumber(); //disable async output message
            new Delay(100);
            if (!WriteDutCommand("VNRRG,03")) return LineNumber();  //send IMU SN command
            if (!UartCheckLineContains(DUTSerialPort, "IMU Serial Number", "$VNRRG,03")) return LineNumber(); //get IMU SN

            if (!WriteDutCommand("VNRRG,04")) return LineNumber();  //send IMU FW version command
            if (!UartCheckLineContains(DUTSerialPort, "IMU FW version", "$VNRRG,04")) return LineNumber(); //get IMU FW version
           
            if (!WriteDutCommand("VNASY,1")) return LineNumber(); //enable async output message
            new Delay(100);
            
            if (!UartCheckLineContains(DUTSerialPort, "IMU serial port", "$VNYMR")) return LineNumber();

            if (!(DAQ.RelaisOpen(308, 0, "_Disable Octopus by removing V_SYS from IMU_PWR"))) return LineNumber();

            // 2.6 - GPS
            LogStr(" >>> Testing 2.6: GPS power, detection, reset and UART communication (receiving)\r\n", 2);
            if (!(DAQ.RelaisClose(306, 200, "_Enable Octopus by connecting V_SYS to GPS_PWR"))) return LineNumber();

            
            for (int i = 5; i > 0; i--)
            {
                LogStr("Waiting for GPS Voltage to settle " + i.ToString("D") + " seconds remaining\r\n", 2);
                new Delay(1000);
            }
            if (!(PSU.GetCurrent(0F, 0.08F, "GPS: VBAT current, no software, Octopus and GPS enabled (to GND at 3.7V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(107, 3.20F, 3.40F, "V_GPS"))) return LineNumber();

            // 2.6.2 - GPS_RST
            if (!(DAQ.RelaisClose(305, 100, "Connect GPS_RST to V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(105, -.05F, .05F, "/GPS_RST, low"))) return LineNumber();
            if (!(DAQ.RelaisOpen(305, 100, "Disconnect GPS_RST from V_SYS"))) return LineNumber();
            if (!(DAQ.GetVoltage(105, 2.1F, 3.4F, "/GPS_RST, high"))) return LineNumber();

            // 2.6.3, 2.6.4 - NEO detection and comms
            if (DAQ.GetVoltage(106, 3.20F, 3.40F, "GPS_NEO"))
            {
                LogStr("Limpet/NEO detected\r\n", 2);
                if (!(DAQ.RelaisClose(305, 100, "_Connect GPS_RST to V_SYS"))) return LineNumber();
                if (!SelectUartChannel(0)) return LineNumber();
                if (!OpenDutCom(9600)) return LineNumber();
                if (!(DAQ.RelaisOpen(305, 100, "_Disconnect GPS_RST from V_SYS"))) return LineNumber();
                if (!(UartCheckLineContains(DUTSerialPort, "GPS: u-Blox(R) NEO-7P serial port 1", "u-blox"))) return LineNumber();
            }
            else
            {
                LogStr("Septentrio detected\r\n", 2);
                if (!(DAQ.RelaisClose(305, 100, "_Connect GPS_RST to V_SYS"))) return LineNumber();
                if (!SelectUartChannel(0)) return LineNumber();
                if (!OpenDutCom(115200)) return LineNumber();
                if (!(DAQ.RelaisOpen(305, 100, "_Disconnect GPS_RST from V_SYS"))) return LineNumber();
                if (!(UartCheckLineContains(DUTSerialPort, "GPS: Septentrio serial port 1", "$TE Septentrio"))) return LineNumber();

                if (!(DAQ.RelaisClose(305, 100, "_Connect GPS_RST to V_SYS"))) return LineNumber();
                if (!SelectUartChannel(1)) return LineNumber();
                if (!OpenDutCom(115200)) return LineNumber();
                if (!(DAQ.RelaisOpen(305, 100, "_Disconnect GPS_RST from V_SYS"))) return LineNumber();
                if (!(UartCheckLineContains(DUTSerialPort, "GPS: Septentrio serial port 2", "$TE Septentrio"))) return LineNumber();
            }
            if (!(DAQ.RelaisOpen(306, 0, "_Disable Octopus by removing V_SYS from GPS_PWR"))) return LineNumber();

            // 2.7 - Cell modem
            if (chkTestingLisa.Checked)
            {
                LogStr(" >>> Testing 2.7: Cell modem UART communication (transmitting and receiving)\r\n", 2);
                int i;
                if ((i = CheckCellModem()) != 0)
                {
                    LogStr(" >>> Cell modem test failed -> retrying\r\n", 2);
                    CloseUart(LisaSerialPort);
                    if (!(DAQ.RelaisClose(212, 0, "Tie LISA_RST to BAT_P again"))) return LineNumber();
                    new Delay(2000);
                    if ((i = CheckCellModem()) != 0) return i;
                }
            }
            else LogStr(">>> No Cell modem fitted, skipping test <<<", 2);

            // 2.8 - Controller
            LogStr(" >>> Testing 2.8: Programming and checking protection controller (receiving)\r\n", 2);
            if (!(DAQ.GetVoltage(112, 2.4F, 2.92F, "VCC from VBAT, no software (to GND"))) return LineNumber();

            // 2.8.2 - Controller programming
            if (chkProgramFirmware.Checked)
            {
                if (!(CallProgJlink('E'))) return LineNumber();
                if (!(CallProgJlink('A'))) return LineNumber();
            }

             // 2.8.3 - Controller comms
            if (!(SelectUartChannel(2))) return LineNumber();
            if (!(DAQ.RelaisClose(307, 200, "_Enable Octopus by connecting V_SYS to LASER_PWR (to generate supply for controller UART)"))) return LineNumber();
            if (!OpenDutCom(115200)) return LineNumber();
            if (!CallProgJlink('R')) return LineNumber();
            if (!UartCheckLineContains(DUTSerialPort, "Protection controller boot message", "Protection")) return LineNumber();

            // 2.8.4 - Controller firmware version
            if (!WriteAndCheckCommand("STM", "STM,OK")) return LineNumber();
            if (!(ReadStoreDutString("FWR", "Firmware version, application"))) return LineNumber();

            // 2.8.5 - Controller alternative supply and return
            ResetPsuAndDmm();
            if (!(SimulateBareBattery(0.08F))) return LineNumber();
            if (!(PSU.GetCurrent(0F, 0.005F, "VBAT current, with software, Octopus off (to VBAT_N at 3.7V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(112, 2.5F, 2.8F, "VCC from VBAT, with software (to VBAT_N"))) return LineNumber();

            if (!(PSU.SetVoltage(0F, 0.1F, -0.1F, 0.1F, "_Removing simulated battery (to VBAT_N)", 0))) return LineNumber();


            // 2.9 - External input protection
            // Preparation
            ResetPsuAndDmm();

            if (!(DAQ.RelaisClose(307, 200, "_Enable Octopus by connecting V_SYS to LASER_PWR (to generate supply for controller UART)"))) return LineNumber();
           
            if (!(SimulateBattery(0.08F))) return LineNumber();
            if (!(SelectUartChannel(2))) return LineNumber();
            //if (!CallProgJlink('R')) return LineNumber();
            new Delay(500);
     
            if (!WriteAndCheckCommand("TMS,0", "TMS,OK")) return LineNumber();
            new Delay(500);

            // 2.9.1 - Reverse protection
            LogStr(" >>> Testing 2.9: Protection for external supply\r\n", 2);
            if (!(SimulateExternalPower(0.005F))) return LineNumber();
            if (!(DAQ.RelaisClose(201, 0, "_Reverse polarity for P1"))) return LineNumber();
            if (!(DAQ.RelaisClose(202, 0, "Reverse polarity for P1"))) return LineNumber();
            if (!(PSU.SetVoltage(1.0F, 0.005F, 0.95F, 1.05F, "_Vin (to GND), applied (1.0V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(115, -0.1F, 0.1F, "V_IN_RP with -1.0V Vin (to GND"))) return LineNumber();
            if (!(DAQ.RelaisOpen(201, 0, "_Normal polarity for P1"))) return LineNumber();
            if (!(DAQ.RelaisOpen(202, 0, "Normal polarity for P1"))) return LineNumber();
            if (!(PSU.SetVoltage(8.0F, 0.05F, 7.9F, 8.1F, "_Vin (to GND), applied (8.0V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(116, 1.3F, 2.3F, "Voltage at TP63 with 8.0V Vin (to GND"))) return LineNumber();

            // 2.9.2 - Overvoltage protection
            if (!(DAQ.GetVoltage(117, 1.3F, 2.3F, "Voltage at TP61 with 8.0V Vin (to GND"))) return LineNumber();
            if (!(PSU.SetVoltage(9.0F, 0.05F, 8.9F, 9.1F, "_Vin (to GND), applied (9.0V)", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(118, -0.1F, 0.1F, "VBUS with 9.0V Vin (to GND"))) return LineNumber();

            // Charger and Buck converter
            ResetPsuAndDmm();
            new Delay(1000);
            if (!(SimulateExternalPower(0.1F))) return LineNumber();
            if (!(PSU.SetVoltage(5.0F, 0.10F, 4.9F, 5.1F, "_Vin (to GND), applied (5.0V)", 0))) return LineNumber();
            if (!(DAQ.RelaisClose(307, 1000, "_Enable Octopus by connecting V_SYS to LASER_PWR"))) return LineNumber();
             // With no battery, Vsys can be anywhere between 3.65V and 4.35V (datasheet, page 18). Adding 0.1V margin
            if (!(DAQ.GetVoltage(101, 3.55F, 4.45F, "V_SYS from charger, battery disconnected"))) return LineNumber();
            if (!(DAQ.GetVoltage(102, 3.20F, 3.40F, "V_U2C from charger, no load"))) return LineNumber();

            if (!(SimulateBareBattery(0.08F))) return LineNumber();
            if (!(DAQ.RelaisOpen(203, 0, "_Link DAQ_COM to GND"))) return LineNumber();

            // 2.13 - part 1
            // Read temperatures without charger being plugged in. Test mode and charger can give slightly off-values
            if (!OpenDutCom(115200)) return LineNumber();
            if (!(SelectUartChannel(2))) return LineNumber();
            if (!CallProgJlink('R')) return LineNumber();
            new Delay(500);
            if (!WriteAndCheckCommand("STM", "STM,OK")) return LineNumber();
            if (!(DAQ.RelaisClose(210, 200, "_Set NTC1 to 70degC"))) return LineNumber();
            new Delay(2000);
            if (!ReadCheckNumber("TR", 62, 82, "Temperature reading at 70degC, with charger")) return LineNumber();
            if (!(DAQ.RelaisOpen(210, 200, "_Set NTC1 to 70degC"))) return LineNumber();

            // 2.10 - Battery discharge temperature shut-off
            LogStr(" >>> Testing 2.10/2.11: Battery charging and themal protection\r\n", 2);
            ResetPsuAndDmm();
            if (!(SimulateBareBattery(0.08F))) return LineNumber();
            if (!(DAQ.RelaisOpen(203, 0, "_Link DAQ_COM to GND"))) return LineNumber();

            // Simulate charger to enable Octopus protection chip
            if (!(DAQ.RelaisClose(211, 200, "_Connect PSU1 to Vin"))) return LineNumber();
            if (!(PSU.SelectChannel(1, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(5.0F, 2.2F, 4.5F, 5.1F, "_Applying Vin to enable protection chip (5.0V)", 0))) return LineNumber();
            new Delay(2000);
            if (!(PSU.SetVoltage(0.0F, 0.1F, -0.1F, 0.1F, "_Removing Vin", 0))) return LineNumber();
            // Wait 1s for voltage to settle
            new Delay(1000);
            if (!(DAQ.GetVoltage(113, 3.6F, 3.8F, "Discharging NTC result, normal (to GND)"))) return LineNumber();
            // enable UART comms to MCU
            if (!(SelectUartChannel(2))) return LineNumber();
            if (!(DAQ.RelaisClose(307, 200, "_Enable Octopus by connecting V_SYS to LASER_PWR (to generate supply for controller UART)"))) return LineNumber();
            if (!OpenDutCom(115200)) return LineNumber();
            if (!CallProgJlink('R')) return LineNumber();
            if (!UartCheckLineContains(DUTSerialPort, "_Protection controller boot message", "Protection")) return LineNumber();
            if (!WriteAndCheckCommand("STM", "STM,OK")) return LineNumber();

            // 2.11 - Charger
            // 2.11.1 - LDO, assuming P1 is set up, just increase voltage and disable P2
            if (!(PSU.SetVoltage(5.5F, 2.2F, 5.4F, 5.6F, "_Applying Vin for charging (5.5V, 2.2A max)", 0))) return LineNumber();
            if (!(PSU.SelectChannel(2, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(0.0F, 0.1F, -0.1F, 4.2F, "_Removing simulated battery", 0))) return LineNumber();
            if (!(DAQ.GetVoltage(112, 2.6F, 2.9F, "VCC from REGN, with software (to GND"))) return LineNumber();

            // 2.11.2 - Pre-charge current
            if (!(DAQ.RelaisClose(207, 200, "_Simulate battery load with a 4.7R resistor between VBAT and VBAT_N"))) return LineNumber();
            if (!WriteAndCheckCommand("TMS,e", "TMS,OK")) return LineNumber();
            new Delay(500);
            if (!(PSU.SelectChannel(2, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(2.5F, 2.0F, 2.4F, 4.3F, "Battery voltage when charging at Vbat=2.5V", 0))) return LineNumber();
            if (!(PSU.SelectChannel(1, 0, 1))) return LineNumber();
            new Delay(1000);
            if (!(PSU.GetCurrent(0.003F, 0.15F, "Vin current for charging battery at Vbat=2.5V", 0))) return LineNumber();

            // 2.11.3 - Charge current
            if (!(PSU.SelectChannel(2, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(3.5F, 2.0F, 3.4F, 4.3F, "Battery voltage when charging at Vbat=3.5V", 0))) return LineNumber();
            if (!(PSU.SelectChannel(1, 0, 1))) return LineNumber();
            new Delay(1000);
            if (!(PSU.GetCurrent(0.58F, 0.93F, "Vin current for charging battery at Vbat=3.5V", 0))) return LineNumber();
            
            // 2.11.4 - Voltage regulation
            if (!(PSU.SelectChannel(2, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(4.15F, 0.5F, 4.05F, 4.3F, "Battery voltage when charging at Vbat=4.15V", 0))) return LineNumber();
            if (!(PSU.SelectChannel(1, 0, 1))) return LineNumber();
            new Delay(1000);
            if (!(PSU.GetCurrent(0.3F, 0.6F, "Vin current for charging battery at Vbat=4.15V", 0))) return LineNumber();

            // 2.13 - part 2: Charging temperature shut-off
            // 2.13.1 - Charging temperature shut-off (normal temp)
            if (!(DAQ.RelaisOpen(207, 200, "Removing battery load (4.7R resistor between VBAT and VBAT_N)"))) return LineNumber();
            new Delay(1000);
            if (!(DAQ.RelaisClose(207, 200, "Adding battery load again(4.7R resistor between VBAT and VBAT_N)"))) return LineNumber();
            new Delay(500);

            if (!(PSU.SelectChannel(2, 0, 1))) return LineNumber();
            if (!(PSU.SetVoltage(4.15F, 1.5F, 4.05F, 4.3F, "Battery voltage when charging at Vbat=4.15V", 0))) return LineNumber();
            if (!(PSU.SelectChannel(1, 0, 1))) return LineNumber();
            new Delay(200);
            if (!(PSU.GetCurrent(0.3F, 0.6F, "Vin current for charging battery at Vbat=4.15V", 0))) return LineNumber();

            // 2.13.2 - Charging temperature shut-off (high temp)
            if (!(DAQ.RelaisClose(210, 200, "_Set NTC1 to 70degC"))) return LineNumber();
            new Delay(2000);
            if (!(PSU.GetCurrent(0.0F, 0.020F, "Vin current for charging battery at Vbat=4.15V at 70degC", 0))) return LineNumber();

            if (!(DAQ.RelaisOpen(210, 200, "_Set NTC1 to 25degC"))) return LineNumber();
            new Delay(500);
            if (!(PSU.SetVoltage(0F, 2.2F, -0.2F, 0.2F, "_Charger voltage off", 0))) return LineNumber();
            new Delay(500);
            if (!ReadCheckNumber("TR", 20, 30, "Temperature reading at 25degC, no charger")) return LineNumber();
            if (!(PSU.SetVoltage(5.5F, 2.2F, 5.4F, 5.6F, "_Applying Vin for charging (5.5V, 2.2A max)", 0))) return LineNumber();
            new Delay(200);

            if (!ReadCheckNumber("TR", 20, 30, "Temperature reading at 25degC, with charger")) return LineNumber();
            if (!(PSU.GetCurrent(0.3F, 0.6F, "Vin current for charging battery at Vbat=4.15V back at 25degC", 0))) return LineNumber();

            if (!WriteAndCheckCommand("TMS,q", "TMS,OK")) return LineNumber();
            new Delay(500);

            // 2.12.1 - All off
            if (!ReadCheckNumber("VRU", 0, 15, "VBUS_USB, all off")) return LineNumber();
            if (!ReadCheckNumber("VRO", 0, 15, "VBUS_OTG, all off")) return LineNumber();
            if (!ReadCheckNumber("VRD", 47, 60, "VBUS_DCIN, all off")) return LineNumber();

            // 2.12.2 Power bypass to H1
            if (!WriteAndCheckCommand("TMS,1", "TMS,OK")) return LineNumber();
            new Delay(500);
            if (!ReadCheckNumber("VRU", 0, 15, "VBUS_USB, DC bypass")) return LineNumber();
            if (!ReadCheckNumber("VRO", 47, 60, "VBUS_OTG, DC bypass")) return LineNumber();
            if (!ReadCheckNumber("VRD", 47, 60, "VBUS_DCIN, DC bypass")) return LineNumber();

            // 2.12.3 Power bypass to USB
            if (!WriteAndCheckCommand("TMS,2", "TMS,OK")) return LineNumber();
            new Delay(500);
            if (!ReadCheckNumber("VRU", 47, 60, "VBUS_USB, USB bypass")) return LineNumber();
            if (!ReadCheckNumber("VRO", 47, 60, "VBUS_OTG, USB bypass")) return LineNumber();
            if (!ReadCheckNumber("VRD", 47, 60, "VBUS_DCIN, USB bypass")) return LineNumber();
            if (!WriteAndCheckCommand("TMS,0", "TMS,OK")) return LineNumber();

            return MAIN_LOOP_PASSED;
        }
        #endregion

    }
}